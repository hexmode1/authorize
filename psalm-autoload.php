<?php
$IP = getenv( "MW_INSTALL_PATH" );
if ( $IP === false || !is_file( "$IP/includes/AutoLoader.php" ) ) {
	echo "Set the environment variable MW_INSTALL_PATH!\n";
	exit;
}

// global $wgLanguageCode;
// $wgLanguageCode = "en";
// global $wgVersion;
// $wgVersion = "XXX";

define( "MEDIAWIKI", true );
require_once "$IP/includes/AutoLoader.php";
// require_once "$IP/maintenance/Maintenance.php";
require_once "$IP/includes/Defines.php";
require_once "$IP/includes/DefaultSettings.php";
require_once "$IP/includes/GlobalFunctions.php";
