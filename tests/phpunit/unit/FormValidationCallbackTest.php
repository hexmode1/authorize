<?php

/**
 * Copyright (C) 2020  MediaWiki Stakeholders
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Marijn van Wezel <marijnvanwezel@gmail.com>
 */

use Authorize\Specials\Includes\Country;
use Authorize\Specials\Includes\FormValidationCallback;
use Authorize\Specials\SpecialSubscribeCorporate;
use MediaWiki\MediaWikiServices;
use PHPUnit\Framework\TestCase;

/**
 * Class FormSubmitCallbackTest
 *
 * @group Callbacks
 * @covers FormValidationCallback
 */
class FormValidationCallbackTest extends TestCase {
	private FormValidationCallback $form_validation_callback;

	public function setUp() : void {
		parent::setUp();
		$this->form_validation_callback = new FormValidationCallback();
	}

	public function testFundraisingAmountValidationCallback() {
		$config = MediaWikiServices::getInstance()->getMainConfig();

		// Absolute minimum is '1'
		$min = max( $config->get( 'WSAuthorizeMinFundraisingAmount' ), 1 );
		// Absolute maximum is '5000'
		$max = min( $config->get( 'WSAuthorizeMaxFundraisingAmount' ), 5000 );

		foreach ( range( $min, $max ) as $value ) {
			$this->assertTrue(
				$this->form_validation_callback->
				fundraisingAmountValidationCallback( $value, [] )
			);
		}

		foreach ( range( 0, $min - 1 ) as $value ) {
			$this->assertNotTrue(
				$this->form_validation_callback->
				fundraisingAmountValidationCallback( $value, [] )
			);
		}

		foreach ( range( $max + 1, 100000 ) as $value ) {
			$this->assertNotTrue(
				$this->form_validation_callback->
				fundraisingAmountValidationCallback( $value, [] )
			);
		}

		$this->assertNotTrue(
			$this->form_validation_callback->
			fundraisingAmountValidationCallback( "", [] )
		);
	}

	public function testCreditCardCVVValidationCallback() {
		for ( $length = 1; $length < 100; $length++ ) {
			for ( $i = 0; $i < 1000; $i++ ) {
				$r = rand( 0, (int)str_repeat( "9", $length ) );
				$r = str_pad( $r, $length, "0" );

				$result = $this->form_validation_callback
						->creditCardCVVValidationCallback( $r, [] );

				if ( $length === 3 || $length === 4 ) {
					$this->assertTrue( $result );
				} else {
					$this->assertNotTrue( $result );
				}
			}
		}

		$this->assertNotTrue(
			$this->form_validation_callback->
			creditCardCVVValidationCallback( "", [] )
		);
	}

	public function testCreditCardExpiryValidationCallback() {
		$current_year = (int)date( 'Y' );

		$valid_years = range( $current_year, $current_year + 50 );
		$valid_months = range( 1, 12 );

		for ( $year = 0; $year < 10000; $year++ ) {
			for ( $month = 0; $month < 100; $month++ ) {
				$result = $this->form_validation_callback
						->creditCardExpiryValidationCallback( "$month/$year", [] );

				if (
					in_array( $year, $valid_years )
					&& in_array( $month, $valid_months )
				) {
					$this->assertTrue( $result );
				} else {
					$this->assertNotTrue( $result );
				}
			}
		}

		$this->assertNotTrue(
			$this->form_validation_callback->
			creditCardExpiryValidationCallback( "", [] )
		);
	}

	public function testCreditCardNumberValidationCallback() {
		$ae_credit_cards = json_decode( file_get_contents(
			__DIR__ . "/ae_random_test_credit_numbers.json"
			), true );
		$discover_credit_cards = json_decode( file_get_contents(
			__DIR__ . "/discover_random_test_credit_numbers.json"
		), true );
		$mc_credit_cards = json_decode( file_get_contents(
			__DIR__ . "/mc_random_test_credit_numbers.json"
		), true );
		$visa_credit_cards = json_decode( file_get_contents(
			__DIR__ . "/visa_random_test_credit_numbers.json"
		), true );

		foreach ( $ae_credit_cards as $cc ) {
			$number = $cc["CreditCard"]["CardNumber"];
			$this->assertTrue(
				$this->form_validation_callback
				->creditCardNumberValidationCallback( (string)$number, [] )
			);
		}

		foreach ( $discover_credit_cards as $cc ) {
			$number = $cc["CreditCard"]["CardNumber"];
			$this->assertTrue(
				$this->form_validation_callback->
				creditCardNumberValidationCallback( (string)$number, [] )
			);
		}

		foreach ( $mc_credit_cards as $cc ) {
			$number = $cc["CreditCard"]["CardNumber"];
			$this->assertTrue(
				$this->form_validation_callback->
				creditCardNumberValidationCallback( (string)$number, [] )
			);
		}

		foreach ( $visa_credit_cards as $cc ) {
			$number = $cc["CreditCard"]["CardNumber"];
			$this->assertTrue(
				$this->form_validation_callback->
				creditCardNumberValidationCallback( (string)$number, [] )
			);
		}

		for ( $i = 0; $i < 100000; $i++ ) {
			$number = rand( 0, 100000000000000 );
			$this->assertNotTrue(
				$this->form_validation_callback->
				creditCardNumberValidationCallback( (string)$number, [] )
			);
		}

		$this->assertNotTrue(
			$this->form_validation_callback->
			creditCardNumberValidationCallback( "", [] )
		);
	}

	public function testMembershipCorporateValidationCallback() {
		$available_memberships
			= SpecialSubscribeCorporate::getAvailableCorporateMemberships();

		foreach ( $available_memberships as $available_membership ) {
			$this->assertTrue(
				$this->form_validation_callback
				->membershipCorporateValidationCallback( $available_membership, [] )
			);
		}

		$this->assertNotTrue(
			$this->form_validation_callback
			->membershipCorporateValidationCallback( "", [] )
		);
	}

	public function testPeriodValidationCallback() {
		$valid_periods = [ "monthly", "yearly" ];

		foreach ( $valid_periods as $period ) {
			$this->assertTrue(
				$this->form_validation_callback
				->periodValidationCallback( $period, [] )
			);
		}

		$this->assertNotTrue(
			$this->form_validation_callback->periodValidationCallback( "", [] )
		);
	}

	public function testMembershipValidationCallback() {
		$memberships = MediaWikiServices::getInstance()
					 ->getMainConfig()->get( "WSAuthorizeMemberships" );

		foreach ( $memberships as $membership => $data ) {
			if ( isset( $data["individual"] ) && $data["individual"] === true ) {
				$this->assertTrue(
					$this->form_validation_callback
					->membershipValidationCallback( $membership, [] )
				);
				$this->assertNotTrue(
					$this->form_validation_callback
					->membershipCorporateValidationCallback( $membership, [] )
				);
			} else {
				$this->assertNotTrue(
					$this->form_validation_callback
					->membershipValidationCallback( $membership, [] )
				);
				$this->assertTrue(
					$this->form_validation_callback
					->membershipCorporateValidationCallback( $membership, [] )
				);
			}
		}

		$this->assertNotTrue(
			$this->form_validation_callback->membershipValidationCallback( "", [] )
		);
		$this->assertNotTrue(
			$this->form_validation_callback->
			membershipCorporateValidationCallback( "", [] )
		);
	}

	public function testCountryValidationCallback() {
		$countries = Country::getList();

		foreach ( $countries as $country ) {
			$this->assertTrue(
				$this->form_validation_callback
				->countryValidationCallback( $country, [] )
			);
		}

		$this->assertNotTrue(
			$this->form_validation_callback->countryValidationCallback( "", [] )
		);
	}

	public function testZipValidationCallback() {
		$this->assertNotTrue(
			$this->form_validation_callback->zipValidationCallback( "", [] )
		);
	}

	public function testStateValidationCallback() {
		$this->assertNotTrue(
			$this->form_validation_callback->stateValidationCallback( "", [] )
		);
	}

	public function testCityValidationCallback() {
		$this->assertNotTrue(
			$this->form_validation_callback->cityValidationCallback( "", [] )
		);
	}

	public function testAddressValidationCallback() {
		$this->assertNotTrue(
			$this->form_validation_callback->addressValidationCallback( "", [] )
		);
	}

	public function testCompanyValidationCallback() {
		$this->assertTrue(
			$this->form_validation_callback->companyValidationCallback( "", [] )
		);
	}

	public function testLastNameValidationCallback() {
		for ( $i = 1; $i < 100; $i++ ) {
			$string = $this->generateRandomString( $i );
			$this->assertTrue(
				$this->form_validation_callback
				->lastNameValidationCallback( $string, [] )
			);

			$string_parts = str_split( $string );
			$this->arrayInsert( $string_parts, $i, (string)rand( 0, 10000 ) );
			$this->assertNotTrue(
				$this->form_validation_callback
				->lastNameValidationCallback( implode( "", $string_parts ), [] )
			);
		}

		$this->assertNotTrue(
			$this->form_validation_callback->lastNameValidationCallback( "", [] )
		);
	}

	public function testFirstNameValidationCallback() {
		for ( $i = 1; $i < 100; $i++ ) {
			$string = $this->generateRandomString( $i );
			$this->assertTrue(
				$this->form_validation_callback->
				firstNameValidationCallback( $string, [] )
			);

			$string_parts = str_split( $string );
			$this->arrayInsert( $string_parts, $i, (string)rand( 0, 10000 ) );
			$this->assertNotTrue(
				$this->form_validation_callback->
				firstNameValidationCallback( implode( "", $string_parts ), [] )
			);
		}

		$this->assertNotTrue(
			$this->form_validation_callback->firstNameValidationCallback( "", [] )
		);
	}

	private function generateRandomString( $length = 10 ) {
		$characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen( $characters );
		$randomString = '';

		for ( $i = 0; $i < $length; $i++ ) {
			$randomString .= $characters[rand( 0, $charactersLength - 1 )];
		}

		return $randomString;
	}

	private function arrayInsert( &$array, $value, $index ) {
		$array = array_merge(
			array_splice( $array, max( 0, $index - 1 ) ), [ $value ], $array
		);
		return $array;
	}
}
