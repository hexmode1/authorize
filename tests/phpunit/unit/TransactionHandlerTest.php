<?php

/**
 * Copyright (C) 2020  MediaWiki Stakeholders
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Marijn van Wezel <marijnvanwezel@gmail.com>
 */

use Authorize\Membership;
use Authorize\Specials\Includes\BillingDetails;
use Authorize\Specials\Includes\CreditCardDetails;
use Authorize\TransactionHandler;

/**
 * Class TransactionHandlerTest
 *
 * @group Callbacks
 * @covers \Authorize\TransactionHandler
 */
class TransactionHandlerTest extends \PHPUnit\Framework\TestCase {
	public function testGetScheduleIntervalLength() {
		$credit_card_details = $this->createMock( CreditCardDetails::class );

		$billing_details = $this->createMock( BillingDetails::class );
		$billing_details->method( 'getBillingPeriod' )
			->willReturn( BillingDetails::PERIOD_MONTHLY );

		$transaction_handler = new TransactionHandler(
			$billing_details, $credit_card_details
		);

		$this->assertEquals( $transaction_handler->getScheduleIntervalLength(), 30 );

		$billing_details = $this->createMock( BillingDetails::class );
		$billing_details->method( 'getBillingPeriod' )
			->willReturn( BillingDetails::PERIOD_YEARLY );

		$transaction_handler = new TransactionHandler(
			$billing_details, $credit_card_details
		);

		$this->assertEquals( $transaction_handler->getScheduleIntervalLength(), 365 );
	}

	public function testGetScheduleTotalOccurrences() {
		$credit_card_details = $this->createMock( CreditCardDetails::class );
		$billing_details = $this->createMock( BillingDetails::class );

		$transaction_handler = new TransactionHandler(
			$billing_details, $credit_card_details
		);

		$this->assertEquals(
			$transaction_handler->getScheduleTotalOccurrences(), "9999"
		);
	}

	public function testGetExpiryTimestamp() {
		$credit_card_details = $this->createMock( CreditCardDetails::class );

		$billing_details = $this->createMock( BillingDetails::class );
		$billing_details->method( 'getBillingPeriod' )
			->willReturn( BillingDetails::PERIOD_MONTHLY );

		$transaction_handler = new TransactionHandler(
			$billing_details, $credit_card_details
		);

		$billing_details = $this->createMock( BillingDetails::class );
		$billing_details->method( 'getBillingPeriod' )
			->willReturn( BillingDetails::PERIOD_YEARLY );

		$transaction_handler = new TransactionHandler(
			$billing_details, $credit_card_details
		);
	}

	public function testGetDueAmount() {
		$billing_details_monthly = $this->createMock( BillingDetails::class );
		$billing_details_monthly->method( 'getBillingPeriod' )
			->willReturn( BillingDetails::PERIOD_MONTHLY );

		$billing_details_yearly = $this->createMock( BillingDetails::class );
		$billing_details_yearly->method( 'getBillingPeriod' )
			->willReturn( BillingDetails::PERIOD_YEARLY );

		$credit_card_details = $this->createMock( CreditCardDetails::class );

		$transaction_handler_monthly = new TransactionHandler(
			$billing_details_monthly, $credit_card_details
		);
		$transaction_handler_yearly  = new TransactionHandler(
			$billing_details_yearly,  $credit_card_details
		);

		for ( $i = 1; $i < 10000; $i++ ) {
			$membership_mock = $this->createMock( Membership::class );
			$membership_mock->method( 'getCost' )
				->willReturn( $i );

			$due_amount_monthly = $transaction_handler_monthly
								->getDueAmount( $membership_mock );
			$due_amount_yearly  = $transaction_handler_yearly
								->getDueAmount( $membership_mock );

			$this->assertEquals( $due_amount_monthly, (string)ceil( $i / 12 ) );
			$this->assertEquals( $due_amount_yearly,  (string)$i );
		}
	}
}
