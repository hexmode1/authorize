CREATE TABLE /*_*/mwstake_members (
      user_id INT NOT NULL,
      subscription_id CHAR(7) UNIQUE NOT NULL,
      expiry_date BIGINT NOT NULL,
      is_cancelled BOOLEAN NOT NULL DEFAULT FALSE,
      billing_first_name VARCHAR(128),
      billing_last_name VARCHAR(128),
      billing_company VARCHAR(128),
      billing_address VARCHAR(256),
      billing_city VARCHAR(128),
      billing_state VARCHAR(128),
      billing_zip VARCHAR(16),
      billing_country VARCHAR(64),
      billing_period VARCHAR(8),
      PRIMARY KEY ( user_id, subscription_id )
) /*$wgDBTableOptions*/;