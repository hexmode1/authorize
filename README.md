This document covers the features and specification of the WSAuthorize
MediaWiki extension.

This extension was written specifically for MWStakeholders.

The extension uses the PHP library
​[cimpleo/omnipay-authorizenetrecurring](https://github.com/cimpleo/omnipay-authorizenetrecurring)
to set up recurring payments with Visa's Authorize.net.

The extension consists of three special pages, namely:

-   Special:Subscribe​, used for individual subscription and
    cancellation of subscriptions, available to all logged in users
-   Special:SubscribeCorporate​, used for corporate subscription and
    only available to people with the "member" right
-   Special:Fundraising, ​used for donating arbitrary amounts of money
    at any time and available to all logged in users

Special:Subscribe
=================

This special page is available to all logged in users, but will differ
between users. Any logged in user, that is not yet a member, will see a
multi-page form allowing them to subscribe.

This subscription form consists of five parts:

1.  The initial greeting, that tells them that they can become a member
    by filling out this form.
2.  The billing details form, that allows the user to fill in their
    billing details. These billing details will be stored in the session
    during the filling in of the form and will be stored in the database
    once the user has become a member.
3.  The membership selection form, that allows the user to select the
    membership they want. This choice will be stored in the session, but
    not in the database.
4.  The billing period selection form, that allows the user to select a
    billing period. The billing period must either be "yearly" or
    "monthly" (30 days).
5.  The confirmation form, this form will show everything the user
    entered and will allow them to fill in their credit card details.

When the user clicks on "Subscribe", they will immediately be billed and
a subscription will get set up with Authorize.net. After they have been
billed, they will be put in the corresponding user groups and will get
redirected to the page "Special:Subscribe/subscribe/success", confirming
their subscription.

Once a user has become a member, the Special:Subscribe special page will
show a cancellation form instead. This cancellation form consists of one
part and when the user proceeds with the cancellation, they will
immediately be removed from the user groups and their subscription will
get cancelled immediately.

Special:SubscribeCorporate
==========================

This form is only available for members with the "member" right. If the
user does not have this right, they will be redirected to the
Special:Subscribe form.

This form allows any "member" to upgrade their membership to a corporate
membership.

Then a user with only the "member-individual" group looks at the form,
all corporate groups are available. (N.B. a corporate group is a group
defined in "\$wgWSAuthorizeMemberships" that does not have the
"individual" parameter). A user with "member-silver" will only get shown
corporate memberships that are higher than it, so "member-gold" and
"member-platinum".

After a user has chosen the membership upgrade they want, they can click
Next. On this page, they can confirm their choice and enter their credit
card details. When a user clicks subscribe, the following happens:

-   Their current Authorize.net recurring payment subscription will be
    cancelled
-   They will be resubscribed with the new price
-   They will be charged the price of the upgrade immediately
-   They will be added to the appropriate user groups

When a user that has "member-platinum" visits the SubscribeCorporate
special page, they will be redirected to the cancel page.

Special:Fundraising
===================

This page allows any logged in user to donate arbitrary amounts of
money. The minimum and maximum amounts are respectively given by
`$wgWSAuthorizeMinFundraisingAmount` and
`$wgWSAuthorizeMaxFundraisingAmount`, both are respectively also
hard-limited to \$1 and \$5000.

When a user that is not a member visits the Fundraising page, they will
be shown a form to enter their billing details and the amount they want
to donate. The billing details entered here will not be stored, but will
be sent to Authorize.net for administration. When a member visits the
Fundraising page, their billing details are already known and will be
filled in automatically.

After the user clicks "Next", they will be sent to a confirmation screen
where they can confirm their billing details and donation amount and can
enter their credit card details.

After the user clicks "Make donation" a one-time "recurring" payment
will be set up with Authorize.net and their credit card will be charged
once.

Configuration parameters
========================

-   `$wgWSAuthorizeMemberships` Allows admins to create new membership
    tiers.
-   `$wgWSAuthorizeMinFundraisingAmount` ​The minimum fundraising amount
    a user can enter (hard limited to "1" or higher).
-   `$wgWSAuthorizeMaxFundraisingAmount` The maximum fundraising amount
    a user can enter (hard limited to "5000" or lower).
-   `$wgWSAuthorizeAuthName` The Authorize.net API login ID.
-   `$wgWSAuthorizeTransactionKey` ​The Authorize.net API transaction
    key.
-   `$wgWSTestMode` Whether or not to use the Authorize.net testing API
    (default: false).

System messages
===============

I found it quite difficult to think of good messages for some of the
forms and error. Nearly all text seen by the user is defined in terms of
a system message and can be changed by editing the "en.json" file in the
"i18n" folder in the extension directory.

Any system message ending in "-wikitext" supports wikitext markup.

Maintenance scripts
===================

There are two maintenance scripts available. These are located in the
folder "maintenance" in the extension directory.

The maintenance script "cancelMembership.php" cancels the given user's
their membership. It can be invoked as follows:

``` {.shell}
$ php maintenance/cancelMembership.php --user=”<username>”
```

or

``` {.shell}
$ php maintenance/cancelMembership.php --id=”<subscription_id>”
```

The maintenance script `renewMembership.php` renews the given user's
membership.

Since recurring payments are (should!) already be set up with
Authorize.net this script does not communicate with Authorize.net again.
It merely renews the user's entry in the database and adds the length of
their billing period to the group expiry date. It can be invoked as
follows:

``` {.shell}
$ php maintenance/renewMembership.php --user=”<username>”
```

The maintenance script will give a warning when someone tries to renew a
membership twice in a short period of time.

Installation
============

1.  Download the extension
2.  Add the following at the bottom of LocalSettings.php:

``` {.php}
wfLoadExtension( “WSAuthorize” );
```

1.  Run the update script, which will automatically create the necessary
    database tables that this extension needs.
2.  Configure the extension as required.
3.  Done - Navigate to Special:Version to verify that the extension is
    successfully installed.
