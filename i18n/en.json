{
	"@metadata": {
	"authors": [
		"Marijn van Wezel <marijn@wikibase.nl>"
	]
	},
	"authorize": "Authorize",
	"wsa-name": "WSAuthorize",
	"wsa-desc": "MediaWiki extension to set up subscriptions through Authorize.net",
	"wsa-tiers-desc": "This configuration parameter is used to specify the list of available tiers.",
	"wsa-no-permissions-special-description": "Please log in",
	"wsa-special-description": "Become a member",
	"wsa-special-corporate-description": "Become a corporate member",
	"wsa-special-description-success": "Thank you for becoming a member",
	"wsa-special-description-fundraising": "Make your donation now",
	"wsa-special-description-fundraising-success": "Thank you for your donation",
	"wsa-special-description-fail": "Something went wrong",
	"wsa-special-description-cancel": "Cancel your subscription",
	"wsa-special-description-cancel-success": "Your subscription has been cancelled",
	"wsa-no-permissions-special-content": "You need to be logged in to become a member.",
	"wsa-subscribe-form-legend": "Become a member",
	"wsa-subscribe": "Subscribe",
	"wsa-invalid-form-order": "Something went wrong and the form was submitted in an invalid order. This usually happens when you press the 'Back' button in your browser. Please return to the start and try again. Our apologies for the inconvenience.",
	"wsa-subscribe-form-first-name": "First name",
	"wsa-subscribe-form-last-name": "Last name",
	"wsa-subscribe-form-company": "Company",
	"wsa-subscribe-form-address": "Address",
	"wsa-subscribe-form-city": "City",
	"wsa-subscribe-form-state": "State",
	"wsa-subscribe-form-zip": "Zip",
	"wsa-subscribe-form-country": "Country",
	"wsa-subscribe-form-membership": "Choose a membership",
	"wsa-subscribe-form-billing-period": "Choose a billing period",
	"wsa-subscribe-form-membership-info": "Chosen membership",
	"wsa-subscribe-form-billing-period-info": "Billing period",
	"wsa-subscribe-form-credit-card-number": "Enter your credit card number",
	"wsa-subscribe-form-credit-card-expiry": "Enter your credit card expiry date",
	"wsa-subscribe-form-credit-card-cvv": "Enter your credit card CVV",
	"wsa-next": "Next",
	"wsa-nav-introtext": "Navigation ",
	"wsa-nav-start": "Start",
	"wsa-fill-out-field": "Fill out this field",
	"wsa-max-field-length": "This field must not be larger than $1 characters",
	"wsa-field-not-contain-numbers": "This field should not contain any numbers",
	"wsa-field-invalid-characterset": "This field should only consist of the following characters: <code>$1</code>",
	"wsa-billing-section": "Billing details",
	"wsa-membership-section": "Membership",
	"wsa-period-section": "Billing period",
	"wsa-payment-section": "Payment details",
	"wsa-amount-section": "Donation amount (USD)",
	"wsa-corporate-confirm-section": "Membership details",
	"wsa-amount": "Amount",
	"wsa-due": "(due today: $$1)",
	"wsa-credit-card-required": "A credit card is required",
	"wsa-invalid-credit-card": "Please give a valid credit card number",
	"wsa-invalid-membership": "\"$1\" is not a valid membership type.",
	"wsa-invalid-individual-membership": "\"$1\" is not an individual membership level.",
	"wsa-invalid-period": "\"$1\" is not a valid billing period.",
	"wsa-unsupported-or-invalid-credit-card": "This credit card is not supported or the given credit card number is invalid",
	"wsa-expiration-required": "An expiration date is required.",
	"wsa-invalid-expiry-month": "Please give a valid expiry month; it should be of the form <code>MM/YYYY</code> (for example <code>11/2030</code>)",
	"wsa-cvv-required": "A CVV is required",
	"wsa-invalid-cvv": "Please give a valid CVV",
	"wsa-invalid-amount": "Please enter a valid number between $1 and $2",
	"wsa-make-donation": "Make donation",
	"wsa-cancel-form-legend": "We're sorry to see you go",
	"wsa-affirm-cancellation": "I understand the consequences, cancel my subscription and membership.",
	"wsa-cancel": "Cancel subscription",
	"wsa-not-checked": "You must check this checkbox",
	"wsa-subscribe-form-legend-info": "Please fill in your details",
	"wsa-subscribe-form-legend-confirm": "Please confirm your details",
	"wsa-cancel-header": "By cancelling your subscription, you agree that your membership will be terminated immediately and you will lose access to the benefits it provides. You will immediately be removed from the corresponding user groups and you will no longer be billed. You will not get refunded.",
	"wsa-cancel-success-wikitext": "Your subscription and membership are successfully cancelled. You will no longer be billed. Please click [[Main Page|here]] to return to the Main Page.",
	"wsa-subscription-header": "Via this form, you can subscribe to become a member.",
	"wsa-subscription-info-header": "Please fill in your billing details below. These will be stored for later use. If you plan on upgrading to a corporate membership, you should fill in the 'Company' field.",
	"wsa-subscribe-form-legend-membership": "Choose a membership",
	"wsa-subscription-membership-header": "Please choose an <b>individual</b> membership. The membership benefits are listed on <a href='https://mwstake.org/mwstake/wiki/Bylaws/Dues'>Bylaws/Dues</a>",
	"wsa-subscribe-form-legend-period": "Choose a billing period",
	"wsa-subscription-period-header": "Please choose a billing period. If you choose 'Monthly', you will be billed every 30 days.",
	"wsa-subscription-confirm-header": "Please confirm your details below and fill in your credit card details. If you spot any mistake, you should click 'Start' at the top of this page and fill in your details again.",
	"wsa-subscribe-success-wikitext": "You have successfully been made a member.",
	"wsa-fundraising-form-legend": "We ask you, humbly, to help",
	"wsa-fundraising-confirm-form-legend": "Please confirm your details below",
	"wsa-fundraising-confirm-header": "Please confirm your billing details and donation amount below and enter your credit card details.",
	"wsa-fundraising-fail-wikitext": "Unfortunately, something went wrong while trying to process your donation. You can try again by visiting [[Special:Fundraising|this special page]]. You have not been charged.",
	"wsa-fundraising-success-wikitext": "Thank you! Your donation was successfully processed. You can click [[Main Page|here]] to return to the Main Page.",
	"wsa-corporate-subscribe-form-legend-choose-upgrade": "Choose which corporate membership to upgrade to",
	"wsa-corporate-subscribe-choose-upgrade-header": "Please choose the corporate membership you would like to upgrade to.",
	"wsa-corporate-subscribe-form-legend-confirm": "Please confirm your details below",
	"wsa-corporate-subscribe-confirm-header": "Please confirm the chosen membership below and fill in your credit card details. You do not need to re-enter your billing details.",
	"wsa-corporate-subscribe-form-due": "Amount due today",
	"wsa-subscribe-corporate-success-wikitext": "Your membership was successfully upgraded. You can click [[Main Page|here]] to return to the Main Page.",
	"wsa-subscribe-corporate-fail-wikitext": "Sorry, but we were unable to upgrade your membership. You can click 'Start' at the top of this page and try again.",
	"wsa-exception-wikitext": "Internal Error: $1",

	"wsa-subscribe-": "Introduction",
	"wsa-use-test-credit-card": "In test mode, you must use a [https://developer.authorize.net/hello_world/testing_guide.html test credit card].",
	"wsa-subscribe-info": "Information",
	"wsa-subscribe-membership": "Membership",
	"wsa-subscribe-period": "Billing Period",
	"wsa-subscribe-confirm": "Confirmation",
	"wsa-subscribe-fail-wikitext": "Payment gateway reported an error: $1",
	"wsa-membership-subject": "Welcome to MWStake!",
	"wsa-membership-body": "Your card will be billed $1 for $2",
	"wsa-problem-handling-form": "Session Problem",

	"wsa-invalid-gateway": "",
	"wsa-amount-too-small": "The offered amount ($1) is too small to handle!",
	"wsa-amount-too-large": "The offered amount ($1) is more than we can handle!",
	"wsa-membership-group-issue": "Problem getting the group '$1': $2",
	"wsa-transaction-save": "Error while saving: $1",
	"wsa-internal-error": "<span style='border-block-color: black; display: block; color: red; font-weight: bold'>$1</span>",
	"wsa-no-subscription-id": "Subscription request succeeded, but we didn't get a subscription id.",
	"wsa-auth-trans-failure": "Transaction failure: $1",

	"membership": "Membership information",
	"wsa-membership-legend": "",
	"wsa-membership-subscriber": "Subscriber info",
	"wsa-membership-no-login": "You are not logged in.  If you have an account on this wiki, use it to log in.  If you do not have an account, please create one or use a Google login to access this wiki.",
	"wsa-membership-no-subscription": "You are not currently a subscriber.  Please [[Special:Subscribe|subscribe]] and become a member.",
	"wsa-membership-subscription-id": "'''Subscription ID''':",
	"wsa-membership-billing-period": "'''Billing Period''':",
	"wsa-membership-level": "'''Membership Level''':",

	"wsa-go-subscribe": "Set up a subscription!",
	"wsa-go-login": "Login or create account",
	"wsa-go-payment-history": "Check payment history",
	"wsa-no-billing-data": "No billing data is available for this user.",
	"wsa-not-a-country": "\"$1\" is not an acceptable country."
}
