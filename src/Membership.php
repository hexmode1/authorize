<?php

/**
 * Copyright (C) 2020  MediaWiki Stakeholders
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Marijn van Wezel <marijnvanwezel@gmail.com>
 * @author Mark A. Hershberger <mah@nichework.com>
 */

namespace Authorize;

use Authorize\Specials\Includes\BillingDetails;
use MWException;
use MediaWiki\MediaWikiServices;
use RequestContext;
use UnexpectedValueException;
use UserGroupMembership;

class Membership {
	private string $group;
	private int $cost;
	private ?string $dependant;
	private int $vote;
	private bool $individual;

	/**
	 * Creates a new Membership object from the given membership tier key.
	 *
	 * @throws Exception
	 */
	public static function newFromKey( string $membership_key ): Membership {
		$memberships = MediaWikiServices::getInstance()
					 ->getMainConfig()->get( 'WSAuthorizeMemberships' );

		if ( !isset( $memberships[ $membership_key ] ) ) {
			throw new AuthorizeException(
				"The membership `$membership_key` does not exist."
			);
		}

		$membership = $memberships[ $membership_key ];

		if ( !isset( $membership[ 'cost' ] ) ) {
			throw new UnexpectedValueException(
				"The membership `$membership_key` is missing the required `cost` "
				. "attribute."
			);
		}

		$cost = intval( $membership[ 'cost' ] );
		$dependant  = $membership[ 'dependant' ] ?? null;

		if ( $dependant !== null && !isset( $memberships[ $dependant ] ) ) {
			throw new UnexpectedValueException(
				"The membership `$membership_key` has an invalid dependant."
			);
		}

		$permission = $membership[ 'permission' ] ?? null;
		$vote       = intval( $membership[ 'vote' ] );
		$individual = $membership[ 'individual' ] ?? false;

		if ( $cost <= 0 ) {
			throw new UnexpectedValueException( "Amount must be more than zero!" );
		}

		return new Membership(
			$membership_key, $cost, $dependant, $permission, $vote, $individual
		);
	}

	/**
	 * Returns a two-dimensional array of membership arrays.
	 *
	 * @throws ConfigException
	 * @throws MWException
	 */
	public static function getMembershipArrays(): array {
		$memberships = MediaWikiServices::getInstance()
					 ->getMainConfig()->get( 'WSAuthorizeMemberships' );

		if ( empty( $memberships ) ) {
			return [];
		}

		foreach ( $memberships as $key => &$membership ) {
			$membership[ 'localized_name' ]
				= UserGroupMembership::getGroupName( $key );
		}

		return $memberships;
	}

	/**
	 * Get a list of the current user's memberships.
	 *
	 * @throws ConfigException
	 * @throws MWException
	 *
	 * @return array-key[]
	 *
	 * @psalm-return array<int, array-key>
	 */
	public static function getCurrentLoggedUserMemberships(): array {
		$user = RequestContext::getMain()->getUser();
		$groups = $user->getGroups();
		$memberships = array_keys( self::getMembershipArrays() );

		return array_intersect( $memberships, $groups );
	}

	/**
	 * Membership constructor.
	 *
	 * This is not the function you want! Use Membership::newFromKey() instead!
	 */
	public function __construct(
		string $group, int $cost, ?string $dependant = null,
		?string $permission = null, int $vote = 1, bool $individual = false
	) {
		$this->group = $group;
		$this->cost = $cost;
		$this->dependant = $dependant;
		$this->vote = $vote >= 1 ? $vote : 1;
		$this->individual = $individual;
	}

	/**
	 * Returns the key of the group for this membership.
	 */
	public function getGroup(): string {
		return $this->group;
	}

	/**
	 * Returns the cost of this membership.
	 */
	public function getCost(): int {
		return $this->cost;
	}

	/**
	 * Returns the key of the dependant, or NULL if it was not given.
	 */
	public function getDependant(): ?string {
		return $this->dependant;
	}

	/**
	 * Returns the number of votes for this membership.
	 */
	public function getVote(): int {
		return $this->vote;
	}

	/**
	 * Returns true if and only if this Membership is available for individuals.
	 */
	public function isIndividual(): bool {
		return $this->individual;
	}

	/**
	 * Creates a new object of the "dependant" key and returns that, or null
	 * if no dependant is given.
	 *
	 * @throws Exception
	 */
	public function getDependantObject(): ?Membership {
		if ( $this->dependant === null ) {
			return null;
		}
		return self::newFromKey( $this->dependant );
	}

	/**
	 * Returns an array of groups, recursively (or rather, iteratively) taking
	 * the dependant into account.
	 *
	 * @throws Exception
	 *
	 * @return (mixed|string)[]
	 *
	 * @psalm-return array<array-key, mixed|string>
	 */
	public function getGroups(): array {
		$result = [];

		$dependant = $this;
		do {
			$result[] = $dependant->getGroup();
			$dependant = $dependant->getDependantObject();
		} while ( $dependant );

		return $result;
	}

	/**
	 * Return a string representing the amount due for this membership level
	 * per billing period.
	 */
	public function getDueAmount( int $period = BillingDetails::PERIOD_MONTHLY ): string {
		$price = $this->getCost();
		wfDebugLog( "wsauthorize", "Got price: $price" );
		if ( $period === BillingDetails::PERIOD_YEARLY ) {
			return sprintf( "%0.2f", $price );
		}
		if ( $period === BillingDetails::PERIOD_MONTHLY ) {
			return sprintf( "%0.2f", $price / 12 );
		}
		throw new MWException( 'Invalid billing period' );
	}
}
