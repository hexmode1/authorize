<?php

/**
 * Copyright (C) 2020  MediaWiki Stakeholders
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Mark A. Hershberger <mah@nichework.com>
 */
namespace Authorize;

use Authorize\Specials\Includes\BillingDetails;
use User;

class Subscription {
	private ?BillingDetails $bill = null;

	/**
	 * Find this user's billing details.
	 */
	public static function newFromUser( User $user ): ?Subscription {
		$self = new self();

		$self->bill = BillingDetails::newFromUserId( $user->getId() );
		if ( $self->bill === null ) {
			return null;
		}
		return $self;
	}

	/**
	 * Find the billing details via the subscription id
	 */
	public static function newFromId( string $id ): ?Subscription {
		$self = new self();

		$self->bill = BillingDetails::newFromSubscriptionId( $id );
		if ( $self->bill === null ) {
			return null;
		}
		return $self;
	}

	/**
	 * Get the subscription id
	 */
	public function getId(): ?string {
		if ( $this->bill ) {
			return $this->bill->getSubscriptionId();
		}
		return null;
	}

	/**
	 * Get the period (monthly, yearly) this subscription uses
	 */
	public function getPeriod(): ?string {
		if ( $this->bill ) {
			return $this->bill->getBillingPeriodString();
		}
		return null;
	}

	/**
	 * Get the membership object for this subscription
	 */
	public function getMembership(): ?Membership {
		if ( $this->bill ) {
			return $this->bill->getMembership();
		}
		return null;
	}

	/**
	 * Get the label for this subscription level
	 */
	public function getMembershipLevel(): ?string {
		if ( $this->bill ) {
			return $this->bill->getMembershipLevel();
		}
		return null;
	}
}
