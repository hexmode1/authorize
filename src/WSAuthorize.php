<?php

/**
 * Copyright (C) 2020  MediaWiki Stakeholders
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Marijn van Wezel <marijnvanwezel@gmail.com>
 * @author Mark A. Hershberger <mah@nichework.com>
 */

namespace Authorize;

use Authorize\Specials\Includes\BillingDetails;
use DatabaseUpdater;
use MWException;
use RequestContext;
use Title;
use User;

class WSAuthorize {
	/**
	 * Hook for schema updates
	 *
	 * @param DatabaseUpdater $updater for running DB updates
	 * @see https://www.mediawiki.org/wiki/Manual:Hooks/LoadExtensionSchemaUpdates
	 */
	public static function onLoadExtensionSchemaUpdates(
		DatabaseUpdater $updater
	): void {
		$directory = realpath( __DIR__ . '/../sql' );
		$type = $updater->getDB()->getType();
		$sql_file = sprintf(
			"%s/%s/table_mwstake_members.sql", $directory, $type
		);

		if ( !file_exists( $sql_file ) ) {
			throw new MWException(
				"WSAuthorize does not support database type `$type`."
			);
		}

		$updater->addExtensionTable( 'mwstake_members', $sql_file );
	}

	/**
	 * Get a membership object from the key
	 *
	 * @param string $membership_key index into array
	 * @return Membership
	 */
	public static function getMembershipObject(
		string $membership_key
	): Membership {
		return Membership::newFromKey( $membership_key );
	}

	/**
	 * Redirect to a subpage of the special page
	 */
	public static function redirectToSubpage( string $subpage ): void {
		$title = RequestContext::getMain()->getTitle();
		if ( $title ) {
			$titleTxt = $title->getFullText();
			$titlePart = explode( '/', $titleTxt );

			$url = Title::newFromTextThrow( $titlePart[0] )->getFullUrlForRedirect();

			if ( $subpage !== "" ) {
				$url .= "/$subpage";
			}
			RequestContext::getMain()->getOutput()->redirect( $url );
		}
	}

	/**
	 * Utility to get current user.
	 */
	private static function currentUser(): User {
		return RequestContext::getMain()->getUser();
	}

	/**
	 * Utility to get current registered user's billing details.
	 *
	 * @throws MWException if not registered
	 */
	private static function currentBillingDetails(): BillingDetails {
		$user = RequestContext::getMain()->getUser();
		$billing = BillingDetails::newFromUserId( $user->getId() );
		if ( $billing === null ) {
			throw new MWException( 'This user has not been registered' );
		}
		return $billing;
	}

	/**
	 * Returns true if and only if the current logged in user has the given
	 * membership key.
	 */
	public static function loggedUserHasMembership( string $membership ): bool {
		return in_array( $membership, self::currentUser()->getGroups() );
	}

	/**
	 * Get the billing period the user agreed to
	 */
	public static function getBillingPeriodForCurrentUser(): int {
		return self::currentBillingDetails()->getBillingPeriod();
	}

	/**
	 * Get the label for the currrent billing period
	 */
	public static function getBillingPeriodLabelForCurrentUser(): string {
		return self::currentBillingDetails()->getBillingPeriodString();
	}
}
