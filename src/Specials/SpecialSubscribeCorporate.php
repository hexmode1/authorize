<?php

/**
 * Copyright (C) 2020  MediaWiki Stakeholders
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Marijn van Wezel <marijnvanwezel@gmail.com>
 * @author Mark A. Hershberger <mah@nichework.com>
 */

namespace Authorize\Specials;

use Authorize\AuthorizeException;
use Authorize\Membership;
use Authorize\Specials\Includes\Form;
use Authorize\WSAuthorize;
use Exception;
use Html;
use HtmlArmor;
use Message;
use RequestContext;
use SpecialPage;
use Title;

class SpecialSubscribeCorporate extends SpecialPage {
	private ?string $header = null;

	public function __construct() {
		parent::__construct( 'SubscribeCorporate', 'member' );
		$this->requireLogin();
		$this->mContext = RequestContext::getMain();
	}

	/**
	 * Always doing writes.
	 *
	 * @return true
	 */
	public function doesWrites(): bool {
		return true;
	}

	/**
	 * Get the page description
	 *
	 * @todo use permisison checks instead of description?
	 */
	public function getDescription(): string {
		return $this->msg(
			$this->getUser()->isAnon()
			? 'wsa-no-permissions-special-description'
			: $this->getHeaderMsg()
		)->plain();
	}

	/**
	 * Get the header of the page
	 *
	 * @todo consolidate into getDescription?
	 */
	public function getHeaderMsg(): string {
		switch ( $this->header ) {
			case 'success':
				return 'wsa-special-description-success';
			case 'fail':
			case 'exception':
				return 'wsa-special-description-fail';
			default:
				return 'wsa-special-corporate-description';
		}
	}

	/**
	 * Do a corporate subscription.
	 *
	 * @return void
	 */
	public function execute( $par ) {
		$this->header = $par;
		$this->setHeaders();

		switch ( $par ) {
			case 'success':
				$this->getOutput()->addWikiMsg( 'wsa-subscribe-corporate-success-wikitext' );
				return;
			case 'fail':
				$this->getOutput()->addWikiMsg( 'wsa-subscribe-corporate-fail-wikitext' );
				return;
		}

		$title = Title::newFromTextThrow( "Special:Subscribe" );
		if ( !in_array( 'member', $this->getUser()->getRights() ) ) {
			$this->getOutput()->redirect( $title->getFullUrlForRedirect() );
			return;
		}

		if ( WSAuthorize::loggedUserHasMembership( "member-platinum" ) ) {
			/**
			 * This user already has the highest possible membership,
			 * redirect them to the cancellation form.
			 */
			$this->getOutput()->redirect( $title->getFullUrlForRedirect() );
			return;
		}

		$nav = $this->msg( 'parentheses' )
			->rawParams( $this->getLanguage()->pipeList( [
				$this->getLinkRenderer()->makeLink(
					Title::newFromTextThrow( "Special:SubscribeCorporate" ),
					new HtmlArmor( $this->msg( 'wsa-nav-start' )->plain() )
				)
			] ) )
			->text();
		$nav = $this->msg( 'wsa-nav-introtext' ) . $nav;
		$nav = Html::rawElement(
			'div', [ 'class' => 'mw-wsa-special-navigation' ], $nav
		);
		$this->getOutput()->setSubtitle( $nav );

		$form = new Form( $this->getContext(), $this->getOutput() );

		try {
			switch ( $par ) {
				case '':
					// Show initial greeting form
					$form->getChooseCorporateUpgradeForm(
						$this->getAvailableCorporateMemberships()
					)->show();
					break;
				case 'confirm':
					// Ask for user information
					$form->getConfirmCorporateUpgradeForm()->show();
					break;
				default:
					WSAuthorize::redirectToSubpage( '' );
			}
		} catch ( Exception $e ) {
			wfDebugLog( "wsauthorize", "Got an error: " . $e->getMessage() );
			$this->getOutput()->addHtml(
				Message::newFromKey( "wsa-internal-error", $e->getMessage() )->parse()
			);
		}
	}

	/**
	 * @throws AuthorizeException
	 * @throws ConfigException
	 * @throws MWException
	 */
	public static function getAvailableCorporateMemberships(): array {
		$corporate_memberships = self::getCorporateMemberships();
		$current_memberships = Membership::getCurrentLoggedUserMemberships();

		// Take disjunctive union of the values of the membership groups the
		// user already has and the available corporate membership groups,
		// leaving us with the groups the user does not yet have.
		foreach ( $corporate_memberships as $key => $group ) {
			if ( in_array( $group, $current_memberships ) ) {
				unset( $corporate_memberships[ $key ] );
			}
		}

		return $corporate_memberships;
	}

	/**
	 * Returns an associative array containing the corporate membership
	 * tiers, with a human-readable name as the key and the internal group
	 * name as the value.
	 *
	 * @return array-key[]
	 *
	 * @throws ConfigException
	 * @throws AuthorizeException
	 * @throws MWException
	 *
	 * @psalm-return array<string, array-key>
	 */
	public static function getCorporateMemberships(): array {
		$memberships = Membership::getMembershipArrays();

		$corporate_memberships = array_filter(
			$memberships,
			function ( $membership ) {
				if (
					isset( $membership['individual'] )
					&& $membership['individual'] === true
				) {
					return false;
				}
				return true;
			}
		);

		$buffer = [];
		foreach ( $corporate_memberships as $membership => $object ) {
			if ( !isset( $object['cost'] ) ) {
				throw new AuthorizeException(
					"Membership `$membership` has no cost attribute"
				);
			}

			$key = sprintf(
				"%s ($%s)", $object[ 'localized_name' ], $object['cost']
			);
			$buffer[$key] = $membership;
		}

		return $buffer;
	}
}
