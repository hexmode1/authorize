<?php

/**
 * Copyright (C) 2020  MediaWiki Stakeholders
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Mark A. Hershberger <mah@nichework.com>
 */
namespace Authorize\Specials;

use Authorize\Subscription;
use FormSpecialPage;
use HTMLForm;
use RequestContext;
use Status;

class Membership extends FormSpecialPage {
	private ?Subscription $sub = null;
	private ?string $submitButton = null;

	public function __construct() {
		parent::__construct( "Membership" );
		$this->mContext = RequestContext::getMain();
	}

	/**
	 * Give the parent methods the prefix for messages that they need.
	 */
	public function getMessagePrefix() {
		return "wsa-membership";
	}

	/**
	 * The "form fields" when user is not logged in
	 */
	private function getLoginFormFields(): array {
		$this->submitButton = "wsa-go-login";
		return [
			"no-login" => [
				"type" => "info",
				"label-message" => "wsa-membership-no-login"
			],
			"login" => [
				"type" => "hidden",
				"default" => "now"
			]
		];
	}

	/**
	 * The "form fields" when we don't have a subscription
	 */
	private function getNoSubFormFields(): array {
		$this->submitButton = "wsa-go-subscribe";
		return [
			"no-subscription" => [
				"type" => "info",
				"label-message" => "wsa-membership-no-subscription"
			],
			"sign-up" => [
				"type" => "hidden",
				"default" => "now"
			]
		];
	}

	/**
	 * The "form fields" when we have a subscription
	 */
	private function getSubFormFields(): array {
		// Can never happen--see calling function--but makes psalm happy
		if ( !$this->sub ) {
			return [];
		}
		$this->submitButton = "wsa-go-payment-history";
		return [
			"membership" => [
				"section" => "subscriber",
				"type" => "info",
				"label-message" => "wsa-membership-level",
				"default" => $this->sub->getMembershipLevel(),
			],
			"billing_period" => [
				"section" => "subscriber",
				"type" => "info",
				"label-message" => "wsa-membership-billing-period",
				"default" => $this->sub->getPeriod(),
			],
			"subscription" => [
				"section" => "subscriber",
				"type" => "info",
				"label-message" => "wsa-membership-subscription-id",
				"default" => $this->sub->getId(),
			]
		];
	}

	/**
	 * Give the parent methods the form
	 */
	protected function getFormFields(): array {
		$prefix = $this->getMessagePrefix();
		$this->sub = Subscription::newFromUser( $this->getUser() );

		if ( !$this->getUser()->isLoggedIn() ) {
			return $this->getLoginFormFields();
		}

		if ( $this->sub === null ) {
			return $this->getNoSubFormFields();
		}

		return $this->getSubFormFields();
	}

	/**
	 * Set the submit button to the text desired
	 */
	protected function alterForm( HTMLForm $form ): void {
		if ( $this->submitButton !== null ) {
			$form->setSubmitTextMsg( $this->submitButton );
		}
	}

	/**
	 * Handle submission.... redirect, etc
	 */
	public function onSubmit( array $data ): Status {
		if ( isset( $data["login"] ) ) {
			$this->getOutput()->redirect(
				self::getTitleFor( "UserLogin" )->getFullURLForRedirect( [
					"returnto" => self::getTitleFor( $this->getName() )->getPrefixedDBkey()
				] )
			);
		}
		if ( isset( $data["sign-up"] ) ) {
			$this->getOutput()->redirect(
				self::getTitleFor( "Subscribe" )->getFullURLForRedirect( [
					"returnto" => self::getTitleFor( $this->getName() )->getPrefixedDBkey()
				] )
			);
		}

		return Status::newGood();
	}
}
