<?php

/**
 * Copyright (C) 2020  MediaWiki Stakeholders
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Marijn van Wezel <marijnvanwezel@gmail.com>
 * @author Mark A. Hershberger <mah@nichework.com>
 */

namespace Authorize\Specials\Includes;

use Authorize\Membership;
use Authorize\TransactionHandler;
use Authorize\WSAuthorize;
use Exception;
use MediaWiki\Session\Session;
use MediaWiki\Session\SessionManager;
use MediaWiki\MediaWikiServices;
use OutputPage;
use RequestContext;
use Status;
use User;

class FormSubmitCallback {
	const GREETING   = 1;
	const INFO       = 2;
	const MEMBERSHIP = 3;
	const PERIOD     = 4;
	const CORPORATE_UPGRADE = 10;
	const FUNDRAISING = 20;

	const INFO_DATA = 'authorize-form-info-data';
	const MEMBERSHIP_DATA = 'authorize-form-membership-data';
	const PERIOD_DATA = 'authorize-form-period-data';
	const CORPORATE_MEMBERSHIP_DATA
		= 'authorize-form-corporate-membership-data';
	const FUNDRAISING_DATA = 'authorize-form-fundraising-data';
	const FUNDRAISING_AMOUNT = 'authorize-form-fundraising-amount';

	private Session $session;
	private OutputPage $out;
	private bool $individual;

	/**
	 * Constructor for the callback handler
	 */
	public function __construct( OutputPage $out, string $type = null ) {
		$this->session = SessionManager::getGlobalSession();
		$this->out = $out;
		$this->individual = $type === "individual";
	}

	/**
	 * Just send them to the info page.
	 *
	 * @return true
	 */
	public function greetingCallback( array $data ): bool {
		self::cleanSession();

		WSAuthorize::redirectToSubpage( 'info' );

		return true;
	}

	/**
	 * Store their input information
	 *
	 * @return true
	 */
	public function infoCallback( array $data ): bool {
		// At this point, we can assume all data is validated.
		$this->session->set( self::INFO_DATA, [
			'first_name' => $data[ 'first_name' ],
			'last_name' => $data[ 'last_name' ],
			'company' => $data[ 'company' ],
			'address' => $data[ 'address' ],
			'city' => $data[ 'city' ],
			'state' => $data[ 'state' ],
			'zip' => $data[ 'zip' ],
			'country' => $data[ 'country' ]
		] );

		if ( !$this->individual ) {
			WSAuthorize::redirectToSubpage( 'membership' );
		} else {
			$this->session->set( self::MEMBERSHIP_DATA, [
				'membership' => 'member-individual'
			] );
			WSAuthorize::redirectToSubpage( 'period' );
		}

		return true;
	}

	/**
	 * Store their selected period.
	 *
	 * @return true
	 */
	public function choosePeriodCallback( array $data ): bool {
		// At this point, we can assume all data is validated.
		$this->session->set( self::PERIOD_DATA, [
			'billing_period' => $data[ 'billing_period' ]
		] );

		WSAuthorize::redirectToSubpage( 'confirm' );

		return true;
	}

	/**
	 * Handle a transaction after all this is confirmed.
	 *
	 * @throws Exception
	 */
	public function confirmCallback( array $data ): Status {
		$user = RequestContext::getMain()->getUser();
		$credit_card = new CreditCardDetails(
			$data[ 'credit_card_number' ],
			$data[ 'credit_card_expiry' ],
			$data[ 'credit_card_cvv' ]
		);

		$billing_details = new BillingDetails(
			$user->getId(),
			$data[ 'first_name' ],
			$data[ 'last_name' ],
			$data[ 'address' ],
			$data[ 'city' ],
			$data[ 'state' ],
			$data[ 'zip' ],
			$data[ 'country' ],
			$data[ 'billing_period' ] ?? 'once',
			$data[ 'company' ],
			$data[ 'membership' ]
		);

		$membership = Membership::newFromKey( $data[ 'membership' ] );
		$transaction_handler = new TransactionHandler(
			$billing_details, $credit_card
		);
		$result = $transaction_handler->handleSubscriptionRequest(
			$membership
		);

		if ( $result->isOk() ) {
			wfDebugLog(
				"wsauthorize", "confirmCallback success: "
				. var_export( $data, true )
			);
			$this->sendSuccess(
				$user,
				$data['billing period'] ?? 'once',
				$membership->getCost()
			);
			WSAuthorize::redirectToSubpage( "subscribe/success" );
		} else {
			wfDebugLog(
				"wsauthorize", "confirmCallback failure: "
				. var_export( $data, true )
			);
		}

		return $result;
	}

	/**
	 * Cancel the subscription handler.
	 */
	public function cancelCallback( array $data ): Status {
		try {
			$result = TransactionHandler::unsubscribeCurrentUser();
			WSAuthorize::redirectToSubpage( 'cancel/success' );
		} catch ( Exception $e ) {
			$result = Status::newFatal( $e->getMessage() );
		}

		return $result;
	}

	/**
	 * Handle the fundraising input.
	 *
	 * @throws Exception
	 *
	 * @return true
	 */
	public function fundraisingCallback( array $data ): bool {
		self::cleanSession();

		$user = RequestContext::getMain()->getUser();
		$billingDetailsObject = BillingDetails::newFromUserId( $user->getId() );
		$billingDetails = $billingDetailsObject !== null
						? $billingDetailsObject
						: new BillingDetails(
							$user->getId(),
							$data[ 'first_name' ],
							$data[ 'last_name' ],
							$data[ 'address' ],
							$data[ 'city' ],
							$data[ 'state' ],
							$data[ 'zip' ],
							$data[ 'country' ],
							$data[ 'billing_period' ] ?? "once",
							$data[ 'company' ]
						);

		$this->session->set(
			self::FUNDRAISING_DATA, serialize( $billingDetails )
		);
		$this->session->set( self::FUNDRAISING_AMOUNT, $data[ 'amount' ] );

		WSAuthorize::redirectToSubpage( 'confirm' );

		return true;
	}

	/**
	 * Actually perform the fundraising transaction.
	 *
	 * @return true
	 */
	public function fundraisingConfirmCallback( array $data ): bool {
		if (
			!$this->session->get( self::FUNDRAISING_DATA )
			|| !$this->session->get( self::FUNDRAISING_AMOUNT )
		) {
			wfDebugLog(
				"wsauthorize", "Invalid session: " . var_export( $data, true )
			);
			WSAuthorize::redirectToSubpage( '' );
		}

		$billingDetails = unserialize( $this->session->get(
			self::FUNDRAISING_DATA
		) );
		$credit_card = new CreditCardDetails(
			$data[ 'credit_card_number' ],
			$data[ 'credit_card_expiry' ],
			$data[ 'credit_card_cvv' ]
		);

		$amount = $this->session->get( self::FUNDRAISING_AMOUNT );

		$config = MediaWikiServices::getInstance()->getMainConfig();

		// Absolute minimum is '1'
		$min = max( $config->get( 'WSAuthorizeMinFundraisingAmount' ), 1 );
		// Absolute maximum is '5000'
		$max = min( $config->get( 'WSAuthorizeMaxFundraisingAmount' ), 5000 );

		if ( $min > $amount ) {
			throw new Exception( "Amount should be at least $min." );
		}
		if ( $amount > $max ) {
			throw new Exception( "Amount can be no more than $min." );
		}

		$transaction_handler = new TransactionHandler(
			$billingDetails, $credit_card
		);
		$result = $transaction_handler->handleFundraisingRequest( $amount );

		if ( $result->isOk() ) {
			wfDebugLog(
				"wsauthorize", "transaction succeeded!"
			);
			$this->sendSuccess(
				RequestContext::getMain()->getUser(),
				$data['billing period'] ?? 'once', $amount
			);
			WSAuthorize::redirectToSubpage( 'success' );
		} else {
			// @FIXME notify user of what the problem is
			wfDebugLog(
				"wsauthorize", "transaction failed ($result)!"
			);
			WSAuthorize::redirectToSubpage( 'fail' );
		}

		return true;
	}

	/**
	 * Send an email on success.
	 */
	protected function sendSuccess( User $user, string $period, int $amount ): void {
		$user->sendMail(
			wfMessage( 'wsa-membership-subject' )->text(),
			wfMessage( 'wsa-membership-body' )->params( $period, $amount )->text()
		);
	}

	/**
	 * Get corp upgrade input.
	 */
	public function chooseCorporateUpgradeCallback( array $data ): void {
		 self::cleanSession();

		 $this->session->set( self::CORPORATE_MEMBERSHIP_DATA, [
			 'membership' => $data[ 'membership' ]
		 ] );

		WSAuthorize::redirectToSubpage( 'confirm' );
	}

	/**
	 * Handle the corporate upgrade transaction.
	 *
	 * @throws Exception
	 */
	public function confirmCorporateUpgradeCallback( array $data ): Status {
		$user = RequestContext::getMain()->getUser();
		$membership = Membership::newFromKey( $data[ 'membership' ] );
		$billingDetails = BillingDetails::newFromUserId( $user->getId() );

		if ( $billingDetails === null ) {
			return Status::newFatal( "wsa-no-billing-data" );
		}

		$credit_card = new CreditCardDetails(
			$data[ 'credit_card_number' ],
			$data[ 'credit_card_expiry' ],
			$data[ 'credit_card_cvv' ]
		);

		$transaction_handler = new TransactionHandler(
			$billingDetails, $credit_card
		);
		$result = $transaction_handler->handleCorporateUpgradeRequest(
			$membership
		);

		if ( $result->isOk() ) {
			WSAuthorize::redirectToSubpage( 'success' );
		} else {
			$this->out->addHtml( $result->getHtml() );
		}

		return $result;
	}

	/**
	 * Cleans up the created session variables.
	 */
	public static function cleanSession(): void {
		$session = SessionManager::getGlobalSession();

		$session->remove( self::INFO_DATA );
		$session->remove( self::MEMBERSHIP_DATA );
		$session->remove( self::PERIOD_DATA );
		$session->remove( self::FUNDRAISING_DATA );
		$session->remove( self::FUNDRAISING_AMOUNT );
	}
}
