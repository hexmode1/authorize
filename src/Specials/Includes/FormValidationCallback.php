<?php

/**
 * Copyright (C) 2020  MediaWiki Stakeholders
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
o * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Marijn van Wezel <marijnvanwezel@gmail.com>
 * @author Mark A. Hershberger <mah@nichework.com>
 */

namespace Authorize\Specials\Includes;

use Authorize\Membership;
use Authorize\Specials\SpecialSubscribeCorporate;
use Exception;
use MediaWiki\MediaWikiServices;
use Status;

class FormValidationCallback {
	/**
	 * Validate firstname input.
	 */
	public function firstNameValidationCallback(
		?string $fieldName, array $formData
	) {
		if ( empty( $fieldName ) ) {
			return wfMessage( "wsa-fill-out-field" );
		}

		if ( preg_match( '/[0-9]/', $fieldName ) ) {
			wfDebugLog(
				"wsauthorize", __METHOD__ . ": non-numbers: $fieldName "
				. var_export( $formData, true )
			);
			return wfMessage( 'wsa-field-not-contain-numbers' );
		}

		if ( strlen( $fieldName ) > 128 ) {
			wfDebugLog(
				"wsauthorize", __METHOD__ . ": too long: $fieldName "
				. var_export( $formData, true )
			);
			return wfMessage( 'wsa-max-field-length', 128 );
		}

		return true;
	}

	/**
	 * Validate the last name input.
	 */
	public function lastNameValidationCallback(
		?string $fieldName, array $formData
	) {
		if ( empty( $fieldName ) ) {
			return wfMessage( "wsa-fill-out-field" );
		}

		if ( preg_match( '/[0-9]/', $fieldName ) ) {
			wfDebugLog(
				"wsauthorize", __METHOD__ . ": non-numbes: $fieldName "
				. var_export( $formData, true )
			);
			return wfMessage( 'wsa-field-not-contain-numbers' );
		}

		if ( strlen( $fieldName ) > 128 ) {
			wfDebugLog(
				"wsauthorize", __METHOD__ . ": too long: $fieldName "
				. var_export( $formData, true )
			);
			return wfMessage( 'wsa-max-field-length', 128 );
		}

		return true;
	}

	/**
	 * Validate a company name.
	 */
	public function companyValidationCallback(
		?string $fieldName, array $formData
	) {
		if ( empty( $fieldName ) ) {
			return true;
		}

		$compChar = '-!0-9a-zA-Z _,';
		if ( preg_match( '/^[' . $compChar . ']+$/', $fieldName ) !== 1 ) {
			wfDebugLog(
				"wsauthorize",  __METHOD__ . ": Something not valid in company name: "
				. $fieldName . var_export( $formData[$fieldName], true )
			);
			return wfMessage( 'wsa-field-invalid-characterset', $compChar );
		}

		if ( strlen( $fieldName ) > 128 ) {
			wfDebugLog(
				"wsauthorize",  __METHOD__ . ": too long: $fieldName "
				. var_export( $formData[$fieldName], true )
			);
			return wfMessage( 'wsa-max-field-length', 128 );
		}

		return true;
	}

	/**
	 * Validate an address input.
	 */
	public function addressValidationCallback(
		?string $fieldName, array $formData
	) {
		if ( empty( $fieldName ) ) {
			return wfMessage( "wsa-fill-out-field" );
		}

		if ( strlen( $fieldName ) > 256 ) {
			return wfMessage( 'wsa-max-field-length', 256 );
		}

		return true;
	}

	/**
	 * Validate a city input.
	 */
	public function cityValidationCallback(
		?string $fieldName, array $formData
	) {
		if ( empty( $fieldName ) ) {
			return wfMessage( "wsa-fill-out-field" );
		}

		if ( strlen( $fieldName ) > 128 ) {
			return wfMessage( 'wsa-max-field-length', 128 );
		}

		return true;
	}

	/**
	 * Validate a state.
	 */
	public function stateValidationCallback(
		?string $fieldName, array $formData
	) {
		if ( empty( $fieldName ) ) {
			return wfMessage( "wsa-fill-out-field" );
		}

		if ( strlen( $fieldName ) > 128 ) {
			return wfMessage( 'wsa-max-field-length', 128 );
		}

		return true;
	}

	/**
	 * Validate zip codes.
	 */
	public function zipValidationCallback(
		?string $fieldName, array $formData
	) {
		if ( empty( $fieldName ) ) {
			return wfMessage( "wsa-fill-out-field" );
		}

		if ( strlen( $fieldName ) > 16 ) {
			return wfMessage( 'wsa-max-field-length', 16 );
		}

		return true;
	}

	/**
	 * Validate country input.
	 */
	public function countryValidationCallback(
		?string $fieldName, array $formData
	) {
		if ( empty( $fieldName ) ) {
			return wfMessage( "wsa-fill-out-field" );
		}

		$countries = Country::getList();

		if ( !in_array( $fieldName, $countries ) ) {
			return wfMessage( "wsa-not-a-country", $fieldName );
		}

		if ( strlen( $fieldName ) > 64 ) {
			return wfMessage( 'wsa-max-field-length', 64 );
		}

		return true;
	}

	/**
	 * Validate membership input.
	 */
	public function membershipValidationCallback(
		?string $fieldName, array $formData
	) {
		if ( empty( $fieldName ) ) {
			return wfMessage( "wsa-fill-out-field" );
		}

		if ( strlen( $fieldName ) > 255 ) {
			return wfMessage( 'wsa-max-field-length', 255 );
		}

		try {
			$membership = Membership::newFromKey( $fieldName );
		} catch ( Exception $e ) {
			return wfMessage( "wsa-exception-wikitext", $e->getMessage() );
		}

		return $membership->isIndividual()
			? true
			: wfMessage( "wsa-invalid-individual-membership", $fieldName );
	}

	/**
	 * Validate a period input.
	 */
	public function periodValidationCallback(
		?string $fieldName, array $formData
	) {
		if ( empty( $fieldName ) ) {
			return wfMessage( "wsa-fill-out-field" );
		}

		if ( $fieldName !== "yearly" && $fieldName !== "monthly" ) {
			return wfMessage( "wsa-invalid-period", $fieldName );
		}

		if ( strlen( $fieldName ) > 8 ) {
			return wfMessage( 'wsa-max-field-length', 8 );
		}

		return true;
	}

	/**
	 * Validate a membership input.
	 */
	public function membershipCorporateValidationCallback(
		?string $fieldName, array $formData
	) {
		try {
			$available_memberships
				= SpecialSubscribeCorporate::getAvailableCorporateMemberships();
		} catch ( Exception $e ) {
			return wfMessage( "wsa-exception-wikitext", $e->getMessage() );
		}

		return in_array( $fieldName, $available_memberships )
			? true
			: wfMessage( 'wsa-invalid-membership', $fieldName );
	}

	/**
	 * Validates a test CC number from
	 * https://developer.authorize.net/hello_world/testing_guide.html
	 */
	protected function isTestCardNumber( string $str ): bool {
		$testCards = [
			'370000000000002', '6011000000000012', '3088000000000017',
			'38000000000006', '4007000000027', '4012888818888', '4111111111111111',
			'5424000000000015', '2223000010309703', '2223000010309711'
		];

		if ( in_array( $str, $testCards ) ) {
			return true;
		}
		return false;
	}

	/**
	 * This function validates the given credit card number given in
	 * $fieldName. It can recognize the following issuers:
	 *
	 * - American Express
	 * - Mastercard
	 * - Visa
	 * - Discover
	 *
	 * It first matches the IIN to a list of known IIN's for popular credit
	 * card issuers. If that succeeds, it performs the Luhn algorithm to the
	 * credit card number.
	 *
	 * @see https://en.wikipedia.org/wiki/Luhn_algorithm
	 */
	public function creditCardNumberValidationCallback(
		?string $fieldName, array $formData
	) {
		if ( !$fieldName ) {
			return wfMessage( "wsa-credit-card-required" );
		}
		// To check this is a valid credit card number, we apply the Luhn
		// algorithm.
		// Remove all dashes and spaces from the string
		$config             = MediaWikiServices::getInstance()->getMainConfig();
		$credit_card_number = str_replace( [ "-", " " ], "", $fieldName );
		$test_mode          = $config->get( 'WSAuthorizeTestMode' ) === true;

		if ( $test_mode ) {
			if ( $this->isTestCardNumber( $credit_card_number ) ) {
				return true;
			} elseif ( $credit_card_number !== '' ) {
				return wfMessage( 'wsa-use-test-credit-card' );
			}
		}

		if ( !ctype_digit( $credit_card_number ) ) {
			return wfMessage( 'wsa-invalid-credit-card' );
		}

		$length = strlen( $credit_card_number );

		$iin_range_length_one = (int)substr( $fieldName, 0, 1 );
		$iin_range_length_two = (int)substr( $fieldName, 0, 2 );
		$iin_range_length_four = (int)substr( $fieldName, 0, 4 );
		$iin_range_length_six = (int)substr( $fieldName, 0, 6 );

		if ( $iin_range_length_two === 34 || $iin_range_length_two === 37 ) {
			// American Express
			if ( $length !== 15 ) {
				return wfMessage( 'wsa-invalid-credit-card' );
			}
		} elseif (
			$iin_range_length_four >= 2221 && $iin_range_length_four <= 2720
			xor $iin_range_length_two >= 51 && $iin_range_length_two <= 55
		) {
			// Mastercard
			if ( $length !== 16 ) {
				return wfMessage( 'wsa-invalid-credit-card' );
			}
		} elseif ( $iin_range_length_one === 4 ) {
			// Visa
			if ( $length !== 16 ) {
				return wfMessage( 'wsa-invalid-credit-card' );
			}
		} elseif ( $iin_range_length_four === 6011 xor
			$iin_range_length_six >= 622126 && $iin_range_length_six <= 622925 xor
			$iin_range_length_six >= 624000 && $iin_range_length_six <= 626999 xor
			$iin_range_length_six >= 628200 && $iin_range_length_six <= 628899 xor
			$iin_range_length_two === 64 xor
			$iin_range_length_two === 65
		) {
			// Discover
			if ( $length < 16 || $length > 19 ) {
				return wfMessage( 'wsa-invalid-credit-card' );
			}
		} else {
			return wfMessage( 'wsa-unsupported-or-invalid-credit-card' );
		}

		$parity = $length % 2;

		$total = 0;
		for ( $idx = 0; $idx < $length; $idx++ ) {
			$digit = intval( $credit_card_number[ $idx ] );

			if ( $idx % 2 === $parity ) {
				$digit *= 2;

				if ( $digit > 9 ) {
					$digit -= 9;
				}
			}

			$total += $digit;
		}

		$valid = ( $total % 10 ) === 0;

		return $valid
			? true
			: wfMessage( 'wsa-invalid-credit-card' );
	}

	/**
	 * Validate a CC expiration.
	 */
	public function creditCardExpiryValidationCallback(
		?string $fieldName, array $formData
	) {
		if ( !$fieldName ) {
			return wfMessage( "wsa-expiration-required" );
		}
		$expiry_parts = explode( '/', $fieldName );

		if ( count( $expiry_parts ) !== 2 ) {
			return wfMessage( 'wsa-invalid-expiry-month' );
		}

		list( $month, $year ) = $expiry_parts;

		if ( !$month || !$year ) {
			return wfMessage( 'wsa-invalid-expiry-month' );
		}

		if ( !ctype_alnum( $month ) || !ctype_alnum( $year ) ) {
			return wfMessage( 'wsa-invalid-expiry-month' );
		}

		if (
			(int)$month > 12
			|| (int)$year > (int)date( "Y" ) + 50
			|| (int)$year < (int)date( "Y" )
		) {
			return wfMessage( 'wsa-invalid-expiry-month' );
		}

		return true;
	}

	/**
	 * Validate a CVV.
	 */
	public function creditCardCVVValidationCallback(
		?string $fieldName, array $formData
	) {
		if ( !$fieldName ) {
			return wfMessage( 'wsa-cvv-required' );
		}
		return 1 === preg_match( "/^[0-9]{3,4}$/", $fieldName )
				? true
				: wfMessage( 'wsa-invalid-cvv' );
	}

	/**
	 * Validate a fundraising amount.
	 * @throws ConfigException
	 */
	public function fundraisingAmountValidationCallback(
		?string $fieldName, array $formData
	) {
		$config = MediaWikiServices::getInstance()->getMainConfig();

		// Absolute minimum is '1'
		$min = max( $config->get( 'WSAuthorizeMinFundraisingAmount' ), 1 );
		// Absolute maximum is '5000'
		$max = min( $config->get( 'WSAuthorizeMaxFundraisingAmount' ), 5000 );

		if (
			!ctype_digit( (string)$fieldName )
			|| (int)$fieldName < $min
			|| (int)$fieldName > $max
		) {
			return wfMessage( 'wsa-invalid-amount', $min, $max );
		}

		return true;
	}
}
