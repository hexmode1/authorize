<?php

/**
 * Copyright (C) 2020  MediaWiki Stakeholders
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Marijn van Wezel <marijnvanwezel@gmail.com>
 * @author Mark A. Hershberger <mah@nichework.com>
 */

namespace Authorize\Specials\Includes;

use Authorize\Membership;
use DateInterval;
use DateTime;

class BillingDetails {
	const PERIOD_MONTHLY = 1;
	const PERIOD_YEARLY  = 2;
	const PERIOD_ONCE = -1;

	const DAYS_IN_MONTH = 30;
	const DAYS_IN_YEAR = 365;

	private static string $table = 'mwstake_members';
	private static array $fieldList = [
		"user_id", "subscription_id", "membership_level", "expiry_date", "is_cancelled",
		"billing_first_name", "billing_last_name", "billing_company", "billing_address",
		"billing_city", "billing_state", "billing_zip", "billing_country", "billing_period"
	];

	private int $userId;
	private string $firstName;
	private string $lastName;
	private string $address;
	private string $city;
	private string $state;
	private string $zip;
	private string $country;
	private int $billingPeriod;
	private int $expirationDate;
	private ?string $company;
	private ?string $subscriptionId;
	private ?string $membershipLevel;

	/**
	 * BillingDetails constructor.
	 *
	 * @throws \Exception
	 */
	public function __construct(
		int $userId,
		string $firstName,
		string $lastName,
		string $address,
		string $city,
		string $state,
		string $zip,
		string $country,
		string $billingPeriod,
		?string $company,
		?string $membershipLevel = null,
		?string $subscriptionId = null
	) {
		$this->userId = $userId;
		$this->firstName = $firstName;
		$this->lastName = $lastName;
		$this->address = $address;
		$this->city = $city;
		$this->state = $state;
		$this->zip = $zip;
		$this->country = $country;
		$this->billingPeriod = self::getPeriod( $billingPeriod );
		$this->expirationDate = $this->getExpirationFromPeriod();
		$this->company = $company;
		$this->subscriptionId = $subscriptionId;
		$this->membershipLevel = $membershipLevel;
	}

	/**
	 * Reusable translation of string to int for billing period
	 */
	public static function getPeriod( string $period ): int {
		$billingPeriod = strtolower( $period );
		if ( $billingPeriod === "monthly" ) {
			return self::PERIOD_MONTHLY;
		} elseif ( $billingPeriod === "yearly" ) {
			return self::PERIOD_YEARLY;
		} else {
			return self::PERIOD_ONCE;
		}
	}

	/**
	 * Translate the period to some date in the future. PERIOD_YEARLY will be one year from now,
	 * etc.
	 */
	public function getExpirationFromPeriod(): int {
		# default is one second
		$diff = new DateInterval( "PT1S" );

		switch( $this->billingPeriod ) {
		case self::PERIOD_MONTHLY:
			$diff = new DateInterval( "P1M" );
			break;
		case self::PERIOD_YEARLY:
			$diff = new DateInterval( "P1Y" );
			break;
		}

		return intval( ( new DateTime() )->add( $diff )->format( "U" ) );
	}

	/**
	 * Utility function to used the passed functions to performa a query and return an object.
	 */
	protected static function newFromResult( array $conditions ): ?self {
		$dbr = wfGetDb( DB_MASTER );
		$result = $dbr->select(
			self::$table,
			self::$fieldList,
			$conditions
		);
		$row = $result->current();

		if ( !$row || !is_object( $row ) ) {
			return null;
		}

		return new self(
			$row->user_id,
			$row->billing_first_name,
			$row->billing_last_name,
			$row->billing_address,
			$row->billing_city,
			$row->billing_state,
			$row->billing_zip,
			$row->billing_country,
			$row->billing_period,
			$row->billing_company,
			$row->membership_level,
			$row->expiry_date,
			$row->subscription_id
		);
	}

	/**
	 * Get billing details from subscription id.
	 */
	public static function newFromSubscriptionId( string $subscription ): ?self {
		return self::newFromResult( [
			'subscription_id' => $subscription,
			'is_cancelled' => false
		] );
	}

	/**
	 * Creates a new billing details object from the given user ID, or
	 * returns NULL if this user does not have an entry in the database
	 * (yet).
	 *
	 * @throws \Exception
	 */
	public static function newFromUserId( int $user_id ): ?self {
		return self::newFromResult( [
			'user_id' => $user_id,
			'is_cancelled' => false
		] );
	}

	/**
	 * Membership level.  This is also group information, but is kept here so
	 * we can set the group from it.
	 */
	public function getMembershipLevel(): ?string {
		return $this->membershipLevel;
	}

	/**
	 * Membership object.  Created from the level.
	 */
	public function getMembership(): ?Membership {
		if ( $this->membershipLevel ) {
			return Membership::newFromKey( $this->membershipLevel );
		}
		return null;
	}

	/**
	 * Returns the user id.
	 */
	public function getUserId(): int {
		return $this->userId;
	}

	/**
	 * Returns the first name of the customer.
	 */
	public function getFirstName(): string {
		return $this->firstName;
	}

	/**
	 * Returns the last name of the customer.
	 */
	public function getLastName(): string {
		return $this->lastName;
	}

	/**
	 * Returns the company name the customer entered, or NULL if it is not
	 * available.
	 */
	public function getCompany(): ?string {
		return $this->company;
	}

	/**
	 * Returns the billing address of the customer.
	 */
	public function getAddress(): string {
		return $this->address;
	}

	/**
	 * Returns the billing city of the customer.
	 */
	public function getCity(): string {
		return $this->city;
	}

	/**
	 * Returns the billing state of the customer.
	 */
	public function getState(): string {
		return $this->state;
	}

	/**
	 * Returns the billing ZIP of the customer.
	 */
	public function getZip(): string {
		return $this->zip;
	}

	/**
	 * Returns the country the customer lives in.
	 */
	public function getCountry(): string {
		return $this->country;
	}

	/**
	 * Returns the billing period the user selected. Takes the form of one of
	 * the following constants:
	 *
	 * BillingDetails::PERIOD_YEARLY
	 * BillingDetails::PERIOD_MONTHLY
	 */
	public function getBillingPeriod(): int {
		return $this->billingPeriod;
	}

	/**
	 * Get the printable billing period.
	 */
	public function getBillingPeriodString(): string {
		return $this->billingPeriod === self::PERIOD_YEARLY
								  ? 'yearly'
								  : 'monthly';
	}

	/**
	 * Returns the expiry date of the MediaWiki group as a Unix timestamp,
	 * taking the billing period into account.
	 *
	 * @todo maybe be more precise?
	 */
	public function getExpiryTimestamp(): int {
		return $this->expirationDate;
	}

	/**
	 * Returns the subscription if.
	 */
	public function getSubscriptionId(): ?string {
		return $this->subscriptionId;
	}

	/**
	 * Stores the subscription in the database, to be referenced later.
	 */
	public function storeSubscription(
		int $userId, string $subscriptionId
	): void {
		$this->userId = $userId;
		$this->subscriptionId = $subscriptionId;

		$database = wfGetDB( DB_MASTER );
		$database->insert(
			'mwstake_members',
			[
				'user_id' => $userId,
				'subscription_id' => $subscriptionId,
				'expiry_date' => $this->getExpiryTimestamp(),
				'billing_first_name' => $this->getFirstName(),
				'billing_last_name' => $this->getLastName(),
				'billing_company' => $this->getCompany(),
				'billing_address' => $this->getAddress(),
				'billing_city' => $this->getCity(),
				'billing_state' => $this->getState(),
				'billing_zip' => $this->getZip(),
				'billing_country' => $this->getCountry(),
				'billing_period' => $this->getBillingPeriodString()
			]
		);
	}

	/**
	 * Remove the membership
	 */
	public function removeMembership(): void {
		wfGetDB( DB_MASTER )->update(
			'mwstake_members',
			[ 'is_cancelled' => true ],
			[ 'subscription_id' => $this->getSubscriptionId() ]
		);
	}

	/**
	 * Get the user's subscription
	 */
	public static function getUsersSubscription( int $user_id ): ?string {
		$billingData = self::newFromUserId( $user_id );
		if ( $billingData !== null ) {
			return $billingData->getSubscriptionId();
		}
		return null;
	}
}
