<?php

/**
 * Copyright (C) 2020  MediaWiki Stakeholders
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Marijn van Wezel <marijnvanwezel@gmail.com>
 * @author Mark A. Hershberger <mah@nichework.com>
 */
namespace Authorize\Specials\Includes;

class Country {

	/**
	 * Get an array of countries for the HTML form.
	 *
	 * @return string[]
	 *
	 * @psalm-return array<string, string>
	 */
	public static function getMap(): array {
		$ret = [];
		foreach ( self::getList() as $country ) {
			$ret[$country] = $country;
		}
		return $ret;
	}

	/**
	 * The raw list of countries.
	 *
	 * @return string[]
	 *
	 * @psalm-return non-empty-list<string>
	 */
	public static function getList(): array {
		return [
			"Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra",
			"Angola", "Anguilla", "Antarctica", "Antigua and Barbuda",
			"Argentina", "Armenia", "Aruba", "Australia", "Austria",
			"Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados",
			"Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan",
			"Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island",
			"Brazil", "British Indian Ocean Territory", "Brunei Darussalam",
			"Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon",
			"Canada", "Cape Verde", "Cayman Islands",
			"Central African Republic", "Chad", "Chile", "China",
			"Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros",
			"Congo", "Congo, the Democratic Republic of the", "Cook Islands",
			"Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba",
			"Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica",
			"Dominican Republic", "East Timor", "Ecuador", "Egypt",
			"El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia",
			"Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland",
			"France", "France Metropolitan", "French Guiana", "French Polynesia",
			"French Southern Territories", "Gabon", "Gambia", "Georgia",
			"Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada",
			"Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau",
			"Guyana", "Haiti", "Heard and Mc Donald Islands",
			"Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary",
			"Iceland", "India", "Indonesia", "Iran (Islamic Republic of)",
			"Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan",
			"Kazakhstan", "Kenya", "Kiribati",
			"Korea, Democratic People's Republic of", "Korea, Republic of",
			"Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic",
			"Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya",
			"Liechtenstein", "Lithuania", "Luxembourg", "Macau",
			"Macedonia, The Former Yugoslav Republic of", "Madagascar",
			"Malawi", "Malaysia", "Maldives", "Mali", "Malta",
			"Marshall Islands", "Martinique", "Mauritania", "Mauritius",
			"Mayotte", "Mexico", "Micronesia, Federated States of",
			"Moldova, Republic of", "Monaco", "Mongolia", "Montserrat",
			"Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal",
			"Netherlands", "Netherlands Antilles", "New Caledonia",
			"New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue",
			"Norfolk Island", "Northern Mariana Islands", "Norway", "Oman",
			"Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay",
			"Peru", "Philippines", "Pitcairn", "Poland", "Portugal",
			"Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation",
			"Rwanda", "Saint Kitts and Nevis", "Saint Lucia",
			"Saint Vincent and the Grenadines", "Samoa", "San Marino",
			"Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles",
			"Sierra Leone", "Singapore", "Slovakia (Slovak Republic)",
			"Slovenia", "Solomon Islands", "Somalia", "South Africa",
			"South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka",
			"St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname",
			"Svalbard and Jan Mayen Islands", "Swaziland", "Sweden",
			"Switzerland", "Syrian Arab Republic", "Taiwan, Province of China",
			"Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo",
			"Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey",
			"Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda",
			"Ukraine", "United Arab Emirates", "United Kingdom", "United States",
			"United States Minor Outlying Islands", "Uruguay", "Uzbekistan",
			"Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)",
			"Virgin Islands (U.S.)", "Wallis and Futuna Islands",
			"Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe"
		];
	}
}
