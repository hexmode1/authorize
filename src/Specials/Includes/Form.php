<?php

/**
 * Copyright (C) 2020  MediaWiki Stakeholders
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Marijn van Wezel <marijnvanwezel@gmail.com>
 * @author Mark A. Hershberger <mah@nichework.com>
 */

namespace Authorize\Specials\Includes;

use Authorize\AuthorizeException;
use Authorize\WSAuthorize;
use Authorize\Membership;
use HTMLForm;
use IContextSource;
use MediaWiki\Session\SessionManager;
use MWException;
use OutputPage;
use RequestContext;

final class Form {
	private IContextSource $context;
	private FormSubmitCallback $submitCallback;
	private FormValidationCallback $validationCallback;
	private bool $individual;
	private int $userId;

	/**
	 * Constructor for the forms.
	 */
	public function __construct(
		IContextSource $context, OutputPage $out, string $type = null
	) {
		$this->submitCallback = new FormSubmitCallback( $out, $type );
		$this->validationCallback = new FormValidationCallback();
		$this->context = $context;
		$this->userId = RequestContext::getMain()->getUser()->getId();
		$this->individual = ( $type === "individual" );
	}

	/**
	 * Get an HTMLForm object for fundraising.
	 */
	public function getFundraisingForm(): HTMLForm {
		$this->submitCallback::cleanSession();
		$billingDetails = BillingDetails::newFromUserId(
			RequestContext::getMain()->getUser()->getId()
		);

		if ( $billingDetails === null ) {
			$fields = $this->getBillingDetailsField( true );
		} else {
			$fields = [];
		}

		$fields['amount'] = [
			"section" => "wsa-amount-section",
			'type' => 'selectorother',
			'options' => [
				'$5' => 5,
				'$10' => 10,
				'$25' => 25,
				'$50' => 50,
				'$100' => 100,
				'$250' => 250
			],
			'validation-callback' =>
			[ $this->validationCallback, 'fundraisingAmountValidationCallback' ]
		];

		$form = HTMLForm::factory( 'ooui', $fields, $this->context );

		$form->setMethod( 'post' );
		$form->setHeaderText( wfMessage( 'wsa-fundraising-header' )->parse() );
		$form->setSubmitTextMsg( 'wsa-next' );
		$form->setWrapperLegendMsg( 'wsa-fundraising-form-legend' );

		$form->setSubmitCallback( [ $this->submitCallback, 'fundraisingCallback' ] );

		return $form;
	}

	/**
	 * Confirmation form for fundraising.
	 */
	public function getFundraisingConfirmationForm(): HTMLForm {
		$session = SessionManager::getGlobalSession();
		$fundraising_data = $session->get( FormSubmitCallback::FUNDRAISING_DATA );
		$amount = $session->get( FormSubmitCallback::FUNDRAISING_AMOUNT );

		if ( !$fundraising_data || !$amount ) {
			throw new AuthorizeException();
		}

		$billingDetails = unserialize( $fundraising_data );
		if ( !( $billingDetails instanceof BillingDetails ) ) {
			throw new MWException(
				"Data Corruption! Expected a BillingDetails object"
			);
		}

		$form = HTMLForm::factory(
			'ooui',
			[
				'credit_card_number' => [
					"section" => "wsa-payment-section",
					'name' => 'creditCard',
					'type' => 'text',
					'label-message' => 'wsa-subscribe-form-credit-card-number',
					'validation-callback' => [
						$this->validationCallback,
						'creditCardNumberValidationCallback'
					],
					'required' => true,
					'placeholder' => '1111-2222-3333-4444'
				],
				'credit_card_expiry' => [
					"section" => "wsa-payment-section",
					'name' => 'creditCardExpiry',
					'type' => 'text',
					'label-message' => 'wsa-subscribe-form-credit-card-expiry',
					'validation-callback' => [
						$this->validationCallback,
						'creditCardExpiryValidationCallback'
					],
					'required' => true,
					'placeholder' => 'MM/YYYY'
				],
				'credit_card_cvv' => [
					"section" => "wsa-payment-section",
					'name' => 'creditCardCVV',
					'type' => 'text',
					'label-message' => 'wsa-subscribe-form-credit-card-cvv',
					'validation-callback' =>
					[ $this->validationCallback, 'creditCardCVVValidationCallback' ],
					'required' => true,
					'placeholder' => '372'
				],
				'first_name' => [
					"section" => "wsa-billing-section",
					'name' => 'billingFirstName',
					'type' => 'text',
					'label-message' => 'wsa-subscribe-form-first-name',
					'default' => $billingDetails->getFirstName(),
					'validation-callback' =>
					[ $this->validationCallback, 'firstNameValidationCallback' ],
					'required' => true,
					'readonly' => true
				],
				'last_name' => [
					"section" => "wsa-billing-section",
					'name' => 'billingLastName',
					'type' => 'text',
					'label-message' => 'wsa-subscribe-form-last-name',
					'default' => $billingDetails->getLastName(),
					'validation-callback' =>
					[ $this->validationCallback, 'lastNameValidationCallback' ],
					'required' => true,
					'readonly' => true
				],
				'company' => [
					"section" => "wsa-billing-section",
					'name' => 'billingCompany',
					'type' => 'text',
					'label-message' => 'wsa-subscribe-form-company',
					'default' => $billingDetails->getCompany(),
					'validation-callback' =>
					[ $this->validationCallback, 'companyValidationCallback' ],
					'required' => false,
					'readonly' => true
				],
				'address' => [
					"section" => "wsa-billing-section",
					'name' => 'billingAddress',
					'type' => 'text',
					'label-message' => 'wsa-subscribe-form-address',
					'default' => $billingDetails->getAddress(),
					'validation-callback' =>
					[ $this->validationCallback, 'addressValidationCallback' ],
					'required' => true,
					'readonly' => true
				],
				'city' => [
					"section" => "wsa-billing-section",
					'name' => 'billingCity',
					'type' => 'text',
					'label-message' => 'wsa-subscribe-form-city',
					'default' => $billingDetails->getCity(),
					'validation-callback' =>
					[ $this->validationCallback, 'cityValidationCallback' ],
					'required' => true,
					'readonly' => true
				],
				'state' => [
					"section" => "wsa-billing-section",
					'name' => 'billingState',
					'type' => 'text',
					'label-message' => 'wsa-subscribe-form-state',
					'default' => $billingDetails->getState(),
					'validation-callback' =>
					[ $this->validationCallback, 'stateValidationCallback' ],
					'required' => true,
					'readonly' => true
				],
				'zip' => [
					"section" => "wsa-billing-section",
					'name' => 'billingZip',
					'type' => 'text',
					'label-message' => 'wsa-subscribe-form-zip',
					'default' => $billingDetails->getZip(),
					'validation-callback' =>
					[ $this->validationCallback, 'zipValidationCallback' ],
					'required' => true,
					'readonly' => true
				],
				'country' => [
					"section" => "wsa-billing-section",
					'name' => 'billingCountry',
					'type' => 'text',
					'label-message' => 'wsa-subscribe-form-country',
					'default' => $billingDetails->getCountry(),
					'validation-callback' =>
					[ $this->validationCallback, 'countryValidationCallback' ],
					'required' => true,
					'readonly' => true
				],
				'amount' => [
					"section" => "wsa-amount-section",
					'name' => 'amount',
					'type' => 'text',
					'default' => $amount,
					'validation-callback' => [
						$this->validationCallback,
						'fundraisingAmountValidationCallback'
					],
					'required' => true,
					'readonly' => true
				],
			],
			$this->context
		);

		$form->setMethod( 'post' );
		$form->setHeaderText( wfMessage( 'wsa-fundraising-confirm-header' )->parse() );
		$form->setSubmitTextMsg( 'wsa-make-donation' );
		$form->setWrapperLegendMsg( 'wsa-fundraising-confirm-form-legend' );

		$form->setSubmitCallback( [
			$this->submitCallback, 'fundraisingConfirmCallback'
		] );

		return $form;
	}

	/**
	 * Get a form for canceling a membership.
	 */
	public function getCancelForm(): HTMLForm {
		$form = HTMLForm::factory(
			'ooui',
			[
				'affirmation' => [
					'type' => 'check',
					'label-message' => 'wsa-affirm-cancellation',
					'validation-callback' =>
					/** @param mixed $value */
					function ( $value, array $data ) {
						return $value === true
									 ? true
									 : wfMessage( "wsa-not-checked" )->parse();
					}
				]
			],
			$this->context
		);

		$form->setMethod( 'post' );
		$form->setHeaderText( wfMessage( 'wsa-cancel-header' )->parse() );
		$form->setSubmitTextMsg( 'wsa-cancel' );
		$form->setWrapperLegendMsg( 'wsa-cancel-form-legend' );

		$form->setSubmitCallback( [ $this->submitCallback, 'cancelCallback' ] );

		return $form;
	}

	/**
	 * Returns the greeting form.
	 */
	public function getGreetingForm(): HTMLForm {
		$form = HTMLForm::factory( 'ooui', [], $this->context );

		$form->setMethod( 'post' );
		$form->setHeaderText( wfMessage( 'wsa-subscription-header' )->parse() );
		$form->setSubmitTextMsg( 'wsa-next' );
		$form->setWrapperLegendMsg( 'wsa-subscribe-form-legend' );

		$form->setSubmitCallback( [ $this->submitCallback, 'greetingCallback' ] );

		return $form;
	}

	/**
	 * Form to get user's information.
	 */
	public function getInfoForm(): HTMLForm {
		$form = HTMLForm::factory(
			'ooui', $this->getBillingDetailsField(), $this->context
		);

		$form->setMethod( 'post' );
		$form->setHeaderText( wfMessage( 'wsa-subscription-info-header' )->parse() );
		$form->setSubmitTextMsg( 'wsa-next' );
		$form->setWrapperLegendMsg( 'wsa-subscribe-form-legend-info' );

		$form->setSubmitCallback( [ $this->submitCallback, 'infoCallback' ] );

		return $form;
	}

	/**
	 * Form to pick a billing period.
	 */
	public function getChoosePeriodForm(): HTMLForm {
		$form = HTMLForm::factory(
			'ooui',
			[
				'billing_period' => [
					'name' => 'billingPeriod',
					'type' => 'radio',
					'label-message' => 'wsa-subscribe-form-billing-period',
					'validation-callback' =>
					[ $this->validationCallback, 'periodValidationCallback' ],
					'options' => [
						'Yearly' => 'yearly',
						'Monthly' => 'monthly'
					],
					'required' => true
				]
			],
			$this->context
		);

		$form->setMethod( 'post' );
		$form->setHeaderText( wfMessage( 'wsa-subscription-period-header' )->parse() );
		$form->setSubmitTextMsg( 'wsa-next' );
		$form->setWrapperLegendMsg( 'wsa-subscribe-form-legend-period' );

		$form->setSubmitCallback( [ $this->submitCallback, 'choosePeriodCallback' ] );

		return $form;
	}

	/**
	 * Form to get confirmation.
	 */
	public function getConfirmationForm(): HTMLForm {
		$session = SessionManager::getGlobalSession();
		$membership = $session->get( FormSubmitCallback::MEMBERSHIP_DATA )[ 'membership' ] ?? null;
		$billingPeriod = $session->get( FormSubmitCallback::PERIOD_DATA )[ 'billing_period' ] ?? null;
		$info = $session->get( FormSubmitCallback::INFO_DATA );
		if ( $info === null || $membership === null || $billingPeriod === null ) {
			throw new MWException( "Session has been lost!" );
		}

		$form = HTMLForm::factory(
			'ooui',
			[
				'credit_card_number' => [
					"section" => "wsa-payment-section",
					'name' => 'creditCard',
					'type' => 'text',
					'label-message' => 'wsa-subscribe-form-credit-card-number',
					'validation-callback' => [
						$this->validationCallback,
						'creditCardNumberValidationCallback'
					],
					'required' => true,
					'placeholder' => '1111-2222-3333-4444'
				],
				'credit_card_expiry' => [
					"section" => "wsa-payment-section",
					'name' => 'creditCardExpiry',
					'type' => 'text',
					'label-message' => 'wsa-subscribe-form-credit-card-expiry',
					'validation-callback' => [
						$this->validationCallback,
						'creditCardExpiryValidationCallback'
					],
					'required' => true,
					'placeholder' => 'MM/YYYY'
				],
				'credit_card_cvv' => [
					"section" => "wsa-payment-section",
					'name' => 'creditCardCVV',
					'type' => 'text',
					'label-message' => 'wsa-subscribe-form-credit-card-cvv',
					'validation-callback' =>
					[ $this->validationCallback, 'creditCardCVVValidationCallback' ],
					'required' => true,
					'placeholder' => '372'
				],
				'first_name' => [
					"section" => "wsa-billing-section",
					'name' => 'billingFirstName',
					'type' => 'text',
					'label-message' => 'wsa-subscribe-form-first-name',
					'default' => $info[ 'first_name' ],
					'validation-callback' =>
					[ $this->validationCallback, 'firstNameValidationCallback' ],
					'required' => true,
					'readonly' => true
				],
				'last_name' => [
					"section" => "wsa-billing-section",
					'name' => 'billingLastName',
					'type' => 'text',
					'label-message' => 'wsa-subscribe-form-last-name',
					'default' => $info[ 'last_name' ],
					'validation-callback' =>
					[ $this->validationCallback, 'lastNameValidationCallback' ],
					'required' => true,
					'readonly' => true
				],
				'company' => [
					"section" => "wsa-billing-section",
					'name' => 'billingCompany',
					'type' => 'text',
					'label-message' => 'wsa-subscribe-form-company',
					'default' => $info[ 'company' ],
					'validation-callback' =>
					[ $this->validationCallback, 'companyValidationCallback' ],
					'required' => false,
					'readonly' => true
				],
				'address' => [
					"section" => "wsa-billing-section",
					'name' => 'billingAddress',
					'type' => 'text',
					'label-message' => 'wsa-subscribe-form-address',
					'default' => $info[ 'address' ],
					'validation-callback' =>
					[ $this->validationCallback, 'addressValidationCallback' ],
					'required' => true,
					'readonly' => true
				],
				'city' => [
					"section" => "wsa-billing-section",
					'name' => 'billingCity',
					'type' => 'text',
					'label-message' => 'wsa-subscribe-form-city',
					'default' => $info[ 'city' ],
					'validation-callback' =>
					[ $this->validationCallback, 'cityValidationCallback' ],
					'required' => true,
					'readonly' => true
				],
				'state' => [
					"section" => "wsa-billing-section",
					'name' => 'billingState',
					'type' => 'text',
					'label-message' => 'wsa-subscribe-form-state',
					'default' => $info[ 'state' ],
					'validation-callback' =>
					[ $this->validationCallback, 'stateValidationCallback' ],
					'required' => true,
					'readonly' => true
				],
				'zip' => [
					"section" => "wsa-billing-section",
					'name' => 'billingZip',
					'type' => 'text',
					'label-message' => 'wsa-subscribe-form-zip',
					'default' => $info[ 'zip' ],
					'validation-callback' =>
					[ $this->validationCallback, 'zipValidationCallback' ],
					'required' => true,
					'readonly' => true
				],
				'country' => [
					"section" => "wsa-billing-section",
					'name' => 'billingCountry',
					'type' => 'text',
					'label-message' => 'wsa-subscribe-form-country',
					'default' => $info[ 'country' ],
					'validation-callback' =>
					[ $this->validationCallback, 'countryValidationCallback' ],
					'required' => true,
					'readonly' => true
				],
				'membership' => [
					"section" => "wsa-membership-section",
					'name' => 'membership',
					'type' => 'text',
					'label-message' => 'wsa-subscribe-form-membership-info',
					'default' => $membership,
					'validation-callback' =>
					[ $this->validationCallback, 'membershipValidationCallback' ],
					'required' => true,
					'readonly' => true
				],
				'billing_period' => [
					"section" => "wsa-period-section",
					'name' => 'billingPeriod',
					'type' => 'text',
					'label-message' => 'wsa-subscribe-form-billing-period-info',
					'default' => $billingPeriod,
					'validation-callback' =>
					[ $this->validationCallback, 'periodValidationCallback' ],
					'required' => true,
					'readonly' => true
				],
			],
			$this->context
		);

		$memberLevel = Membership::newFromKey( $membership );
		$form->setMethod( 'post' );
		$form->setHeaderText(
			wfMessage( 'wsa-subscription-confirm-header' )->parse()
		);
		$form->setSubmitTextMsg( 'wsa-subscribe' );
		$form->setWrapperLegendMsg( 'wsa-subscribe-form-legend-confirm' );
		$form->setSubmitText(
			wfMessage( 'wsa-subscribe' )->parse() . " " .
			wfMessage(
				'wsa-due', $memberLevel->getDueAmount( BillingDetails::getPeriod( $billingPeriod ) )
			)->parse()
		);

		$form->setSubmitCallback( [ $this->submitCallback, 'confirmCallback' ] );

		return $form;
	}

	/**
	 * Form to do a corporate upgrade.
	 */
	public function getChooseCorporateUpgradeForm( array $options ): HTMLForm {
		$form = HTMLForm::factory(
			'ooui',
			[
				'membership' => [
					'name' => 'membership',
					'type' => 'select',
					'label-message' => 'wsa-subscribe-form-membership',
					'default' => '',
					'validation-callback' => [
						$this->validationCallback,
						'membershipCorporateValidationCallback'
					],
					'options' => $options,
					'required' => true
				]
			],
			$this->context
		);

		$form->setMethod( 'post' );
		$form->setHeaderText(
			wfMessage( 'wsa-corporate-subscribe-choose-upgrade-header' )->parse()
		);
		$form->setSubmitTextMsg( 'wsa-next' );
		$form->setWrapperLegendMsg(
			'wsa-corporate-subscribe-form-legend-choose-upgrade'
		);

		$form->setSubmitCallback( [
			$this->submitCallback, 'chooseCorporateUpgradeCallback'
		] );

		return $form;
	}

	/**
	 * Confirm your corporate upgrade.
	 *
	 * @throws MWException
	 */
	public function getConfirmCorporateUpgradeForm(): HTMLForm {
		$session = SessionManager::getGlobalSession();

		$data = $session->get( FormSubmitCallback::CORPORATE_MEMBERSHIP_DATA );
		$membershipKey = $data[ 'membership' ];
		$membership = Membership::newFromKey( $membershipKey );
		$period = WSAuthorize::getBillingPeriodLabelForCurrentUser();
		$amount = $membership->getDueAmount( WSAuthorize::getBillingPeriodForCurrentUser() );

		$form = HTMLForm::factory(
			'ooui',
			[
				'credit_card_number' => [
					"section" => "wsa-payment-section",
					'name' => 'creditCard',
					'type' => 'text',
					'label-message' => 'wsa-subscribe-form-credit-card-number',
					'validation-callback' => [
						$this->validationCallback,
						'creditCardNumberValidationCallback'
					],
					'required' => true,
					'placeholder' => '1111-2222-3333-4444'
				],
				'credit_card_expiry' => [
					"section" => "wsa-payment-section",
					'name' => 'creditCardExpiry',
					'type' => 'text',
					'label-message' => 'wsa-subscribe-form-credit-card-expiry',
					'validation-callback' => [
						$this->validationCallback,
						'creditCardExpiryValidationCallback'
					],
					'required' => true,
					'placeholder' => 'MM/YYYY'
				],
				'credit_card_cvv' => [
					"section" => "wsa-payment-section",
					'name' => 'creditCardCVV',
					'type' => 'text',
					'label-message' => 'wsa-subscribe-form-credit-card-cvv',
					'validation-callback' => [
						$this->validationCallback, 'creditCardCVVValidationCallback'
					],
					'required' => true,
					'placeholder' => '372'
				],
				'membership' => [
					'section' => 'wsa-corporate-confirm-section',
					'name' => 'membership',
					'type' => 'text',
					'label-message' => 'wsa-subscribe-form-membership-info',
					'default' => $membershipKey,
					'validation-callback' => [
						$this->validationCallback,
						'membershipCorporateValidationCallback'
					],
					'required' => true,
					'readonly' => true
				],
				'billing_period' => [
					'section' => 'wsa-corporate-confirm-section',
					'name' => 'billing_period',
					'type' => 'info',
					'label-message' => 'wsa-subscribe-form-billing-period-info',
					'default' => $period,
					'required' => true,
					'readonly' => true
				],
				'amount_due' => [
					'section' => 'wsa-corporate-confirm-section',
					'name' => 'due',
					'type' => 'info',
					'label-message' => 'wsa-corporate-subscribe-form-due',
					'required' => true,
					'default' => "$" . $amount
				],
			],
			$this->context
		);

		$form->setMethod( 'post' );
		$form->setHeaderText(
			wfMessage( 'wsa-corporate-subscribe-confirm-header' )->parse()
		);
		// $form->setSubmitTextMsg( 'wsa-subscribe' );
		$form->setWrapperLegendMsg( 'wsa-corporate-subscribe-form-legend-confirm' );

		$form->setSubmitText(
			wfMessage( 'wsa-subscribe' )->parse() . " " .
			wfMessage( 'wsa-due', $amount )->parse()
		);

		$form->setSubmitCallback( [
			$this->submitCallback, 'confirmCorporateUpgradeCallback'
		] );

		return $form;
	}

	/**
	 * @return (array|bool|string)[][]
	 *
	 * @phpcs:ignore Generic.Files.LineLength.TooLong
	 * @psalm-return array{first_name: array{name: string, type: string, label-message: string, validation-callback: array{0: FormValidationCallback, 1: string}, required: true}, last_name: array{name: string, type: string, label-message: string, validation-callback: array{0: FormValidationCallback, 1: string}, required: true}, company: array{name: string, type: string, label-message: string, validation-callback: array{0: FormValidationCallback, 1: string}, required: false}, address: array{name: string, type: string, label-message: string, validation-callback: array{0: FormValidationCallback, 1: string}, required: true}, city: array{name: string, type: string, label-message: string, validation-callback: array{0: FormValidationCallback, 1: string}, required: true}, state: array{name: string, type: string, label-message: string, validation-callback: array{0: FormValidationCallback, 1: string}, required: true}, zip: array{name: string, type: string, label-message: string, validation-callback: array{0: FormValidationCallback, 1: string}, required: true}, country: array{name: string, type: string, label-message: string, options: array, validation-callback: array{0: FormValidationCallback, 1: string}, required: true}}
	 */
	private function getBillingDetailsField( bool $force_section = false ): array {
		$fields = [
			'first_name' => [
				'name' => 'billingFirstName',
				'type' => 'text',
				'label-message' => 'wsa-subscribe-form-first-name',
				'validation-callback' =>
				[ $this->validationCallback, 'firstNameValidationCallback' ],
				'required' => true
			],
			'last_name' => [
				'name' => 'billingLastName',
				'type' => 'text',
				'label-message' => 'wsa-subscribe-form-last-name',
				'validation-callback' =>
				[ $this->validationCallback, 'lastNameValidationCallback' ],
				'required' => true
			],
			'company' => [
				'name' => 'billingCompany',
				'type' => 'text',
				'label-message' => 'wsa-subscribe-form-company',
				'validation-callback' =>
				[ $this->validationCallback, 'companyValidationCallback' ],
				'required' => false
			],
			'address' => [
				'name' => 'billingAddress',
				'type' => 'text',
				'label-message' => 'wsa-subscribe-form-address',
				'validation-callback' =>
				[ $this->validationCallback, 'addressValidationCallback' ],
				'required' => true
			],
			'city' => [
				'name' => 'billingCity',
				'type' => 'text',
				'label-message' => 'wsa-subscribe-form-city',
				'validation-callback' =>
				[ $this->validationCallback, 'cityValidationCallback' ],
				'required' => true
			],
			'state' => [
				'name' => 'billingState',
				'type' => 'text',
				'label-message' => 'wsa-subscribe-form-state',
				'validation-callback' =>
				[ $this->validationCallback, 'stateValidationCallback' ],
				'required' => true
			],
			'zip' => [
				'name' => 'billingZip',
				'type' => 'text',
				'label-message' => 'wsa-subscribe-form-zip',
				'validation-callback' =>
				[ $this->validationCallback, 'zipValidationCallback' ],
				'required' => true
			],
			'country' => [
				'name' => 'billingCountry',
				'type' => 'select',
				'label-message' => 'wsa-subscribe-form-country',
				'options' => Country::getMap(),
				'validation-callback' =>
				[ $this->validationCallback, 'countryValidationCallback' ],
				'required' => true
			]
		];

		if ( $force_section ) {
			foreach ( $fields as &$field ) {
				$field['section'] = 'wsa-billing-section';
			}
		}

		return $fields;
	}
}
