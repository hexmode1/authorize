<?php

/**
 * Copyright (C) 2020  MediaWiki Stakeholders
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Marijn van Wezel <marijnvanwezel@gmail.com>
 * @author Mark A. Hershberger <mah@nichework.com>
 */

namespace Authorize\Specials\Includes;

/**
 * Class CreditCardDetails
 * @package Authorize\Specials\Includes
 */
class CreditCardDetails {
	/**
	 * @var string
	 */
	private $credit_card_number;

	/**
	 * @var string
	 */
	private $credit_card_expiry;

	/**
	 * @var string
	 */
	private $credit_card_cvv;

	/**
	 * CreditCardDetails constructor.
	 */
	public function __construct(
		string $credit_card_number, string $credit_card_expiry,
		string $credit_card_cvv
	) {
		$this->credit_card_number = $credit_card_number;
		$this->credit_card_expiry = $credit_card_expiry;
		$this->credit_card_cvv    = $credit_card_cvv;
	}

	/**
	 * Returns the credit card number held in this object.
	 */
	public function getCreditCardNumber(): string {
		return $this->credit_card_number;
	}

	/**
	 * Returns the credit card expiry month.
	 */
	public function getCreditCardExpiryMonth(): string {
		return explode( '/', $this->credit_card_expiry )[0];
	}

	/**
	 * Returns the credit card expiry year.
	 */
	public function getCreditCardExpiryYear(): string {
		return explode( '/', $this->credit_card_expiry )[1];
	}

	/**
	 * Returns the credit card CVV.
	 */
	public function getCreditCardCVV(): string {
		return $this->credit_card_cvv;
	}
}
