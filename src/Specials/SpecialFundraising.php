<?php

/**
 * Copyright (C) 2020  MediaWiki Stakeholders
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Marijn van Wezel <marijnvanwezel@gmail.com>
 * @author Mark A. Hershberger <mah@nichework.com>
 */

namespace Authorize\Specials;

use Authorize\Specials\Includes\Form;
use Authorize\WSAuthorize;
use Exception;
use MWException;
use Message;
use RequestContext;
use SpecialPage;

class SpecialFundraising extends SpecialPage {
	private ?string $header = null;

	public function __construct() {
		parent::__construct( 'Fundraising' );
		$this->requireLogin();
		$this->mContext = RequestContext::getMain();
	}

	/**
	 * Returns the header message or permission denied.
	 */
	public function getDescription(): string {
		return $this->msg(
			$this->getUser()->isAnon()
			? 'wsa-no-permissions-special-description'
			: $this->getHeaderMsg()
		)->parse();
	}

	/**
	 * Figures out what the header message is
	 */
	public function getHeaderMsg(): string {
		switch ( $this->header ) {
			case 'success':
				return 'wsa-special-description-fundraising-success';
			case 'fail':
				return 'wsa-special-description-fail';
			default:
				return 'wsa-special-description-fundraising';
		}
	}

	/**
	 * Let's raise some funds!
	 *
	 * @return null|true
	 */
	public function execute( $par ) {
		$this->header = $par;

		$this->setHeaders();

		$form = new Form( $this->getContext(), $this->getOutput() );

		try {
			switch ( $par ) {
				case '':
					// Show initial greeting form
					$form->getFundraisingForm()->show();
					break;
				case 'confirm':
					// Ask the user to confirm their subscription
					$form->getFundraisingConfirmationForm()->show();
					break;
				case 'success':
					$this->getOutput()->addWikiMsg( 'wsa-fundraising-success-wikitext' );
					return true;
				case 'fail':
					$this->getOutput()->addWikiMsg( 'wsa-fundraising-fail-wikitext' );
					return true;
				default:
					WSAuthorize::redirectToSubpage( '' );
			}
		} catch ( Exception $e ) {
			throw new MWException( Message::newFromKey(
				"wsa-exception-wikitext", $e->getMessage()
			)->text() );
		}
	}
}
