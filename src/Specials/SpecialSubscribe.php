<?php

/**
 * Copyright (C) 2020  MediaWiki Stakeholders
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Marijn van Wezel <marijnvanwezel@gmail.com>
 * @author Mark A. Hershberger <mah@nichework.com>
 */

namespace Authorize\Specials;

use Authorize\Specials\Includes\Form;
use Authorize\WSAuthorize;
use ErrorPageError;
use Exception;
use Html;
use HtmlArmor;
use Message;
use RequestContext;
use SpecialPage;
use Title;

class SpecialSubscribe extends SpecialPage {
	private ?string $header = null;

	public function __construct() {
		parent::__construct( 'Subscribe' );
		$this->requireLogin();
		$this->mContext = RequestContext::getMain();
	}

	/**
	 * @return true
	 */
	public function doesWrites(): bool {
		return true;
	}

	/**
	 * Get the page description
	 *
	 * @todo use permisison checks instead of description?
	 */
	public function getDescription(): string {
		return $this->msg(
			$this->getUser()->isAnon()
			? 'wsa-no-permissions-special-description'
			: $this->getHeaderMsg()
		)->plain();
	}

	/**
	 * Get the header of the page
	 *
	 * @todo consolidate into getDescription?
	 */
	public function getHeaderMsg(): string {
		switch ( $this->header ) {
			case 'subscribe/success':
				return 'wsa-special-description-success';
			case 'subscribe/fail':
			case 'exception':
			case 'cancel/fail':
				return 'wsa-special-description-fail';
			case 'cancel':
				return 'wsa-special-description-cancel';
			case 'cancel/success':
				return 'wsa-special-description-cancel-success';
			default:
				return 'wsa-special-description';
		}
	}

	/**
	 * Set up a subscription!
	 *
	 * @return void
	 */
	public function execute( $par ) {
		$is_member = $this->isMember();

		// @FIXME upgrade should be an option here
		$this->header = $is_member && $par === null ? 'cancel' : $par;
		$this->setHeaders();

		if ( $this->showResultMessage( $par ) ) {
			return;
		}

		if ( $is_member ) {
			$this->showCancellationForm();
			return;
		}

		$nav = $this->msg( 'parentheses' )
			->rawParams( $this->getLanguage()->pipeList( [
				$this->getLinkRenderer()->makeLink(
					Title::newFromTextThrow( "Special:Subscribe" ),
					new HtmlArmor( $this->msg( 'wsa-nav-start' )->plain() )
				)
			] ) )
			->text();
		$nav = $this->msg( 'wsa-nav-introtext' ) . $nav;
		$nav = Html::rawElement(
			'div', [ 'class' => 'mw-wsa-special-navigation' ], $nav
		);
		$this->getOutput()->setSubtitle( $nav );

		$this->handleForm( $par );
	}

	/**
	 * Show where the user is along the way.
	 */
	protected function showProgressBar(
		string $formName, array $step, int $onStepNum
	): void {
		$out = $this->getOutput();

		$out->addModules( 'wsa-progress-steps' );
		$out->addJsConfigVars( 'wsaOnStepNum', $onStepNum );

		$out->addHtml(
			Html::openElement( 'div', [ 'id' => 'progressBarWrapper' ] )
			. Html::openElement( 'ul', [ 'id' => 'progressBar' ] ) );
		foreach ( $step as $name ) {
			$msg = 'wsa-' . $formName . '-' . $name;
			$out->addHtml( Html::element(
				"li", [ 'id' => $msg ], Message::newFromKey( $msg )->text()
			) );
		}
		$out->addHtml(
			Html::closeElement( 'ul' ) . Html::closeElement( 'div' )
		);
	}

	/**
	 * Handle the form
	 */
	protected function handleForm( ?string $par ): void {
		$par ??= '';
		$step = [ '', 'info', 'period', 'confirm' ];
		$onStepNum = array_search( $par, $step );
		if ( false === $onStepNum ) {
			$this->getOutput()->redirect( $this->getPageTitle()->getFullUrlForRedirect() );
			return;
		}
		$form = new Form(
			$this->getContext(), $this->getOutput(), 'individual'
		);
		$this->showProgressBar( 'subscribe', $step, $onStepNum );
		try {
			switch ( $par ) {
			case $step[0]:
				// Show initial greeting form
				$form->getGreetingForm()->show();
				break;
			case $step[1]:
				// Ask for user information
				$form->getInfoForm()->show();
				break;
			case $step[2]:
				// Ask the user which billing period they want
				$form->getChoosePeriodForm()->show();
				break;
			case $step[3]:
				// Ask the user to confirm their subscription
				$form->getConfirmationForm()->show();
				break;
			default:
				WSAuthorize::redirectToSubpage( '' );
			}
		} catch ( Exception $e ) {
			wfDebugLog(
				"wsauthorize", "Problem handling form: " . $e->getMessage()
			);
			throw new ErrorPageError(
				"wsa-problem-handling-form", "wsa-exception-wikitext",
				[ $e->getMessage() ]
			);
		}
	}

	/**
	 * Display an approperiate result message.
	 */
	public function showResultMessage( ?string $par ): bool {
		switch ( $par ) {
			case 'subscribe/success':
				$this->getOutput()->addWikiMsg( 'wsa-subscribe-success-wikitext' );
				return true;
			case 'subscribe/fail':
				$this->getOutput()->addWikiMsg( 'wsa-subscribe-fail-wikitext' );
				return true;
			case 'cancel/success':
				$this->getOutput()->addWikiMsg( 'wsa-cancel-success-wikitext' );
				return true;
			case 'cancel/fail':
				$this->getOutput()->addWikiMsg( 'wsa-cancel-fail-wikitext' );
				return true;
			case 'exception':
				$this->getOutput()->addWikiMsg( 'wsa-exception-wikitext' );
				return true;
			default:
				return false;
		}
	}

	/**
	 * Returns true if and only if the user that is currently viewing this
	 * page is a member. A user is a member when they have the "member"
	 * right.
	 */
	public function isMember(): bool {
		return in_array( "member", $this->getUser()->getRights() );
	}

	/**
	 * Get a cancelation form.
	 */
	protected function showCancellationForm(): void {
		(
			new Form( $this->getContext(), $this->getOutput() )
		)->getCancelForm()->show();
	}
}
