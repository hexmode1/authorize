<?php

/**
 * Copyright (C) 2020  MediaWiki Stakeholders
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Marijn van Wezel <marijnvanwezel@gmail.com>
 * @author Mark A. Hershberger <mah@nichework.com>
 */

namespace Authorize;

use Authorize\Specials\Includes\BillingDetails;
use Authorize\Specials\Includes\CreditCardDetails;
use Authorize\Specials\Includes\FormSubmitCallback;
use ConfigException;
use Exception;
use MediaWiki\MediaWikiServices;
use Omnipay\AuthorizeNetRecurring\Objects\Schedule;
use Omnipay\AuthorizeNetRecurring\RecurringGateway;
use Omnipay\Common\CreditCard;
use Omnipay\Common\Message\ResponseInterface;
use Omnipay\Omnipay;
use RequestContext;
use Status;
use User;

class TransactionHandler {
	private BillingDetails $billing;
	private ?CreditCardDetails $creditCard;
	private ?string $subscriptionId = null;

	/**
	 * TransactionHandler constructor.
	 */
	public function __construct(
		BillingDetails $billing,
		?CreditCardDetails $creditCard = null
	) {
		$this->billing = $billing;
		$this->creditCard = $creditCard;
	}

	/**
	 * Get a transaction handler for the current user.
	 */
	public static function getMembership(): TransactionHandler {
		$subId = BillingDetails::newFromUserId( self::getCurrentUserId() );
		if ( $subId ) {
			return new self( $subId );
		}
		throw new Exception( "No membership found" );
	}

	/**
	 * Handles a cancel request to the Special:Authorize special page.
	 *
	 * @throws Exception
	 */
	public static function unsubscribeCurrentUser(): Status {
		$me = self::getMembership();

		return $me->cancelMembership();
	}

	/**
	 * Sends a request to Authorize.net asking it to cancel the membership
	 * for given in the billing details.
	 *
	 * @throws ConfigException
	 */
	private function cancelMembership(): Status {
		wfDebugLog( "wsauthorize", sprintf(
			"Canceling subscription (%s) for %d (current user: %d)",
			$this->billing->getSubscriptionId() ?? "none", $this->billing->getUserId(),
			self::getCurrentUserId()
		) );

		$gateway = $this->getGateway();

		$gateway->cancelSubscription( [
			'subscriptionId' => $this->billing->getSubscriptionId()
		] )->send();
		$this->removeUserFromGroups();
		$this->billing->removeMembership();

		// later some of the above may not be good
		return Status::newGood();
	}

	/**
	 * Handles a subscription request to the Special:Authorize special page.
	 *
	 * @param Membership $membership The Membership the customer wants to
	 *     subscribe to.
	 * @throws Exception
	 */
	public function handleSubscriptionRequest( Membership $membership ): Status {
		FormSubmitCallback::cleanSession();

		$result = $this->subscribeCurrentUser( $membership );

		if ( $result->isOk() ) {
			RequestContext::getMain()->getUser()->addGroup( 'member' );
		}

		return $result;
	}

	/**
	 * Handler for an upgrade request
	 * @throws Exception
	 */
	public function handleCorporateUpgradeRequest(
		Membership $membership
	): Status {
		FormSubmitCallback::cleanSession();

		$user = RequestContext::getMain()->getUser();
		$user_id = $user->getId();

		// Cancel the user's current membership
		$this->cancelMembership();

		$result = $this->subscribeCurrentUser( $membership );

		if ( $result->isOk() ) {
			 $user->addGroup( 'corp-admin' );
		}

		return $result;
	}

	/**
	 * Handles a request to the Special:Fundraising page.
	 *
	 * @throws ConfigException
	 * @throws Exception
	 */
	public function handleFundraisingRequest( int $amount ): Status {
		if ( !$this->creditCard ) {
			return Status::newFatal( "wsa-credit-card-required" );
		}
		// We can safely assume $amount is safe, but just to make absolutely
		// sure no rogue transactions are done, we assert that it is.
		wfDebugLog(
			"wsauthorize", "Got a fundraising submission of $amount!"
		);
		$config = MediaWikiServices::getInstance()->getMainConfig();

		// Absolute minimum is '1'
		$min = max( $config->get( 'WSAuthorizeMinFundraisingAmount' ), 1 );
		// Absolute maximum is '5000'
		$max = min( $config->get( 'WSAuthorizeMaxFundraisingAmount' ), 5000 );

		if ( $amount <= $min ) {
			return Status::newFatal( "wsa-amount-too-small", $amount );
		}
		if ( $amount > $max ) {
			return Status::newFatal( "wsa-amount-too-large", $amount );
		}

		$schedule = new Schedule( [
			'intervalLength'    => "7",
			'intervalUnit'      => "days",
			'startDate'         => $this->getScheduleIntervalStartDate(),
			'totalOccurrences'  => 1
		] );
		try {
			$card = $this->createCard();
		} catch ( Exception $e ) {
			return Status::newFatal( "wsa-credit-card-required" );
		}

		$gateway = $this->getGateway();

		// $status = $this->getStatus( $this->createSubscriptionRequest(
		// $amount, $gateway, $schedule, $card
		// )->send() );

		return Status::newFatal( "not yet implemented" );
	}

	/**
	 * Subscribes the current logged in user to the given Membership, handling
	 * both the communication with Authorize.net as well as the internal store
	 * of details.
	 */
	private function subscribeCurrentUser(
		Membership $membership
	): Status {
		try {
			$card = $this->createCard();
		} catch ( Exception $e ) {
			return Status::newFatal( "wsa-credit-card-required" );
		}
		$userId   = self::getCurrentUserId();
		$schedule = $this->createSchedule();

		try {
			// We get these before the transaction, since these methods can fail.
			$groups = $membership->getGroups();
		} catch ( Exception $exception ) {
			return Status::newFatal(
				"wsa-membership-group-issue", $membership->getGroup(),
				$exception->getMessage()
			);
		}
		$period = $this->billing->getBillingPeriod();
		$amount = $membership->getDueAmount( $this->billing->getBillingPeriod() );
		wfDebugLog( "wsauthorize", "Attempting to bill for $period: " . var_export( $amount, true ) );
		$status = $this->performSubscriptionRequest( $amount, $schedule, $card );

		if ( $status->isOk() && $this->subscriptionId ) {
			try {
				$this->billing->storeSubscription( $userId, $this->subscriptionId );
				$this->assignGroupsToCurrentUser( $groups );
			} catch ( Exception $e ) {
				$this->cancelMembership();
				wfDebugLog(
					"wsauthorize", "Exception while saving transaction: "
					. $e->getMessage()
				);

				$status = Status::newFatal(
					"wsa-transaction-save", $e->getMessage()
				);
			}
		}
		return $status;
	}

	/**
	 * Returns the schedule interval length.  Is either 365 or 30
	 *
	 * @psalm-return 30|365
	 */
	public function getScheduleIntervalLength(): ?int {
		if (
			$this->billing &&
			$this->billing->getBillingPeriod() === BillingDetails::PERIOD_MONTHLY
		) {
			return BillingDetails::DAYS_IN_MONTH;
		} else {
			return BillingDetails::DAYS_IN_YEAR;
		}
	}

	/**
	 * Returns the start date of the subscription, which is always 'now'.
	 */
	public function getScheduleIntervalStartDate(): string {
		// Date can only return string from the implied now.
		return date( 'Y-m-d' );
	}

	/**
	 * 	 * Returns the total occurrences for use in the Schedule object. '9999'
	 * 	 * means an ongoing subscription, which only ends after it has been
	 * 	 * canceled.
	 *
	 * @return string
	 */
	public function getScheduleTotalOccurrences(): string {
		// Ongoing subscription
		return '9999';
	}

	/**
	 * Creates a Schedule object for a subscription request.
	 */
	protected function createSchedule(): Schedule {
		return new Schedule( [
			'intervalLength'    => $this->getScheduleIntervalLength(),
			'intervalUnit'      => "days",
			'startDate'         => $this->getScheduleIntervalStartDate(),
			'totalOccurrences'  => $this->getScheduleTotalOccurrences()
		] );
	}

	/**
	 * Creates a CreditCard object for a subscription request.
	 */
	protected function createCard(): CreditCard {
		if ( !$this->creditCard ) {
			throw new Exception( "Need a card!" );
		}
		return new CreditCard( [
			'number'            => $this->creditCard->getCreditCardNumber(),
			'expiryMonth'       => $this->creditCard->getCreditCardExpiryMonth(),
			'expiryYear'        => $this->creditCard->getCreditCardExpiryYear(),
			'cvv'               => $this->creditCard->getCreditCardCVV(),
			'billingFirstName'  => $this->billing->getFirstName(),
			'billingLastName'   => $this->billing->getLastName(),
			'billingCompany'    => $this->billing->getCompany(),
			'billingAddress'    => $this->billing->getAddress(),
			'billingCity'       => $this->billing->getCity(),
			'billingState'      => $this->billing->getState(),
			'billingZip'        => $this->billing->getZip(),
			'billingCountry'    => $this->billing->getCountry()
		] );
	}

	/**
	 * @throws Exception
	 */
	private function performSubscriptionRequest(
		string $due_amount,
		Schedule $schedule,
		CreditCard $card
	): Status {
		$gateway = $this->getGateway();
		// 32 character random name
		$name = bin2hex( random_bytes( 16 ) );

		$subscription = $gateway->createSubscription( [
			'subscriptionName'  => $name,
			'amount'            => $due_amount,
			'schedule'          => $schedule,
			'card'              => $card
		] );
		$response = $subscription->send();
		$status = $this->getStatus( $response );
		$data = $response->getData();

		if ( !isset( $data['subscriptionId'] ) && $status->isOk() ) {
			$status->fatal( 'wsa-no-subscription-id' );
		}
		if ( isset( $data['subscriptionId'] ) ) {
			$this->subscriptionId = $data['subscriptionId'];
		}
		return $status;
	}

	/**
	 * Assigns the given group to the current logged in user ($wgUser).
	 */
	private function assignGroupsToCurrentUser( array $groups ): void {
		$user = RequestContext::getMain()->getUser();
		$ts = $this->billing->getExpiryTimestamp();
		$expiry = $ts !== null
				? date( "YmdHis", $ts )
				: null;

		foreach ( $groups as $group ) {
			// TODO: Update this to use UserGroupManager instead, since
			// addGroup() will be deprecated in 1.35.
			$user->addGroup( $group, $expiry );
		}
	}

	/**
	 * Removes the groups from the given user.
	 *
	 * @throws ConfigException
	 */
	private function removeUserFromGroups(): void {
		$user = User::newFromId( $this->billing->getUserId() );
		$groups = array_keys(
			MediaWikiServices::getInstance()->getMainConfig()->get(
				"WSAuthorizeMemberships"
			)
		);

		foreach ( $groups as $group ) {
			$user->removeGroup( $group );
		}
	}

	/**
	 * Returns a status object for the transaction.
	 */
	private function getStatus( ResponseInterface $response ) :Status {
		if ( $response->isSuccessful() ) {
			$status = Status::newGood( $response->getMessage() );
		} else {
			$status = Status::newFatal( "wsa-auth-trans-failure", $response->getMessage() );
		}
		wfDebugLog(
			"wsauthorize", ( $status->isGood() ? "Succeeded" : "Failed" ) . ": $status"
		);
		return $status;
	}

	/**
	 * Convenience method to get the current user id.
	 */
	private static function getCurrentUserId(): int {
		return RequestContext::getMain()->getUser()->getId();
	}

	/**
	 * Creates a new GatewayInterface object for
	 * AuthorizeNetRecurring_Recurring.
	 *
	 * @throws ConfigException
	 */
	private function getGateway(): RecurringGateway {
		$config          = MediaWikiServices::getInstance()->getMainConfig();
		$auth_name       = $config->get( 'WSAuthorizeAuthName' );
		$transaction_key = $config->get( 'WSAuthorizeTransactionKey' );
		$test_mode       = $config->get( 'WSAuthorizeTestMode' ) === true;

		$gateway = Omnipay::create( 'AuthorizeNetRecurring_Recurring' );
		if ( !( $gateway instanceof RecurringGateway ) ) {
			throw new GatewayException( "Didn't get a proper RecurringGateway" );
		}
		$gateway->setAuthName( $auth_name );
		$gateway->setTransactionKey( $transaction_key );
		$gateway->setTestMode( $test_mode );

		return $gateway;
	}

	/**
	 * Returns the subscription ID associated with the given user ID, or false
	 * if the user is not subscribed.
	 */
	private function getSubscriptionIdFromUserId( int $user_id ): ?string {
		return $this->billing->getSubscriptionId();
	}
}
