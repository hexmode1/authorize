mw.loader.using( ['mediawiki.util'] ).then( function () {
	mw.hook( 'wikipage.content' ).add(
		function ( $content ) {
			var step = mw.config.get( 'wsaOnStepNum' );
			var stepLi = document.getElementById( 'progressBar' ).children;

			for ( var i = 0; stepLi.length > i; i++  ) {
				if ( i < step ) {
    				stepLi[i].className = 'complete';
				} else if ( i == step ) {
    				stepLi[i].className = 'active';
				} else {
    				stepLi[i].className = '';
				}
			}
		}
	); } );
