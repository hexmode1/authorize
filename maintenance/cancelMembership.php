<?php

/**
 * Copyright (C) 2020  MediaWiki Stakeholders
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Marijn van Wezel <marijnvanwezel@gmail.com>
 */

namespace Authorize\Maintenance;

use Exception;
use Omnipay\AuthorizeNetRecurring\RecurringGateway;
use Omnipay\Omnipay;
use MediaWiki\MediaWikiServices;
use User;

/**
 * Load the required class
 */
if ( getenv( 'MW_INSTALL_PATH' ) !== false ) {
	require_once getenv( 'MW_INSTALL_PATH' ) . '/maintenance/Maintenance.php';
} else {
	require_once __DIR__ . '/../../../maintenance/Maintenance.php';
}

class CancelMembership extends \Maintenance {
	public function __construct() {
		parent::__construct();

		$this->addDescription(
			'Cancel a user\'s membership. This cancels the subscription with '
			. 'Authorize.net and archives their record in the database.'
		);

		$this->addOption(
			'user', 'The username of the user for which you want to cancel the '
			. 'membership.', false, true, 'u'
		);
		$this->addOption(
			'id', 'The subscription ID, as shown in the Authorize.net '
			. 'dashboard, you want to cancel.', false, true, 'i'
		);
	}

	/**
	 * Do the actual work. All child classes will need to implement this
	 *
	 * @return bool|null|void True for success, false for failure. Not returning
	 *   a value, or returning null, is also interpreted as success. Returning
	 *   false for failure will cause doMaintenance.php to exit the process
	 *   with a non-zero exit status.
	 * @throws ConfigException
	 * @throws Exception
	 */
	public function execute() {
		$gateway = self::getGateway();

		$username = $this->getOption( 'user', false );
		$user_id = null;
		$subscription_id = $this->getOption( 'id', false );

		if ( $username !== false ) {
			$user = User::newFromName( $username );

			if ( !$user ) {
				$this->fatalError( "The given username is not valid." );
			}

			$user_id = $user->getId();

			$result = wfGetDB( DB_MASTER )->select(
				'mwstake_members',
				[
					'subscription_id'
				],
				[
					'user_id' => $user_id,
					'is_cancelled' => false
				]
			);

			if ( $result->numRows() < 1 ) {
				$this->fatalError(
					"There is no active subscription associated with the "
					. "given username."
				);
			}

			$subscription_id = $result->current()->subscription_id;
		} elseif ( $subscription_id !== false ) {
			$result = wfGetDB( DB_MASTER )->select(
				'mwstake_members',
				[
					'user_id'
				],
				[
					'subscription_id' => $subscription_id,
					'is_cancelled' => false,
					'expiry_date > ' . time()
				]
			);

			if ( $result->numRows() < 1 ) {
				$this->fatalError(
					"There is no active subscription associated with the given "
					. "subscription ID."
				);
			}

			$user_id = $result->current()->user_id;
		} else {
			$this->fatalError(
				"Please provide a username or subscription ID."
			);
		}

		if ( $gateway === false || !$gateway instanceof RecurringGateway ) {
			return false;
		}

		$this->output(
			"Abort the cancellation with control-c in the next five seconds... "
		);
		$this->countDown( 5 );

		$this->output( "\n" );

		$response = $gateway->cancelSubscription( [
			'subscriptionId' => $subscription_id
		] )->send();

		if ( !$response->isSuccessful() ) {
			$this->fatalError(
				"Unable to cancel membership: `"
				. $response->getMessage() . "`."
			);
		}

		self::removeUserFromGroups( $user_id );
		self::cancelMembership( $user_id );

		$this->output( "Successfully cancelled subscription.\n" );
	}

	/**
	 * Creates a new GatewayInterface object for
	 * AuthorizeNetRecurring_Recurring.
	 *
	 * @return bool|GatewayInterface
	 * @throws ConfigException
	 */
	private static function getGateway() {
		$config = MediaWikiServices::getInstance()->getMainConfig();

		$auth_name       = $config->get( 'WSAuthorizeAuthName' );
		$transaction_key = $config->get( 'WSAuthorizeTransactionKey' );
		$test_mode       = $config->get( 'WSAuthorizeTestMode' ) === true;

		$gateway = Omnipay::create( 'AuthorizeNetRecurring_Recurring' );

		try {
			$gateway->setAuthName( $auth_name );
			$gateway->setTransactionKey( $transaction_key );
			$gateway->setTestMode( $test_mode );
		} catch ( Exception $exception ) {
			return false;
		}

		return $gateway;
	}

	/**
	 * @param $user_id
	 */
	private static function cancelMembership( $user_id ) {
		wfGetDB( DB_MASTER )->update(
			'mwstake_members',
			[ 'is_cancelled' => true ],
			[
				'is_cancelled' => false,
				'user_id' => $user_id
			]
		);
	}

	/**
	 * Removes the groups from the given user.
	 *
	 * @param $user_id
	 */
	private static function removeUserFromGroups( $user_id ) {
		$user = User::newFromId( $user_id );
		$groups = array_keys(
			MediaWikiServices::getInstance()
			->getMainConfig()->get( "WSAuthorizeMemberships" )
		);

		foreach ( $groups as $group ) {
			$user->removeGroup( $group );
		}
	}
}

$maintClass = CancelMembership::class;
require_once RUN_MAINTENANCE_IF_MAIN;
