<?php

/**
 * Copyright (C) 2020  MediaWiki Stakeholders
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Marijn van Wezel <marijnvanwezel@gmail.com>
 */

namespace Authorize\Maintenance;

use DateTime;
use Authorize\Specials\Includes\BillingDetails;
use Maintenance;
use MediaWiki\MediaWikiServices;
use User;

/**
 * Load the required class
 */
if ( getenv( 'MW_INSTALL_PATH' ) !== false ) {
	require_once getenv( 'MW_INSTALL_PATH' ) . '/maintenance/Maintenance.php';
} else {
	require_once __DIR__ . '/../../../maintenance/Maintenance.php';
}

class RenewMembership extends Maintenance {
	public function __construct() {
		parent::__construct();

		$this->addDescription(
			"Renew a user's membership. This renews a user's membership by "
			. "changing the expiry date in the database and renewing the groups. "
			. "It does NOT communicate with Authorize.net."
		);

		$this->addOption(
			'user', 'The username of the user for which you want to renew the '
			. 'membership.', false, true, 'u'
		);
	}

	/**
	 * Do the actual work. All child classes will need to implement this
	 *
	 * @return bool|null|void True for success, false for failure. Not returning
	 *   a value, or returning null, is also interpreted as success. Returning
	 *   false for failure will cause doMaintenance.php to exit the process
	 *   with a non-zero exit status.
	 * @throws Exception
	 */
	public function execute() {
		$username = $this->getOption( 'user', false );

		if ( $username === false ) {
			$this->fatalError( "You must supply a username." );
		}

		$user = User::newFromName( $username );

		if ( $user === null || $user === false ) {
			$this->fatalError( "This user does not exist." );
		}

		$result = wfGetDB( DB_MASTER )->select(
			'mwstake_members',
			[
				'expiry_date',
				'billing_period'
			],
			[
				'user_id' => $user->getId(),
				'is_cancelled' => false,
				'expiry_date > ' . time()
			]
		);

		if ( $result->numRows() < 1 ) {
			$this->fatalError(
				"There is no active subscription associated with the given "
				. "username."
			);
		}

		$row = $result->current();

		if ( $row->expiry_date > self::getBillingPeriodAsSeconds( $row->billing_period ) ) {
			$this->output(
				"This membership will expire in "
				. $this->secondsToTime( $row->expiry_date - time() ) . ". "
				. "This is past the billing period and means this membership "
				. "likely has already been renewed. Press control-c to abort "
				. "the renewal.\n"
			);

			$this->output(
				"Abort the renewal with control-c in the next nine seconds... "
			);
			$this->countDown( 9 );
		} else {
			$this->output(
				"Abort the renewal with control-c in the next five seconds... "
			);
			$this->countDown( 5 );
		}

		$this->output( "\n" );

		$user_id = $user->getId();

		$expiry_date = self::getNewExpiryDate(
			$row->expiry_date, $row->billing_period
		);
		$groups = self::getGroupsForUser( $user );

		self::assignUserToGroups( $groups, $expiry_date, $user_id );
		self::renewDatabaseEntry( $expiry_date, $user_id );

		$this->output( "Membership successfully renewed.\n" );
	}

	/**
	 * Returns the new expiry date as a unix timestamp.
	 *
	 * @param $expiry_date
	 * @param $billing_period
	 * @return int
	 */
	private static function getNewExpiryDate( $expiry_date, $billing_period ) {
		return self::getBillingPeriodAsSeconds( $billing_period, $expiry_date );
	}

	private static function getBillingPeriodAsSeconds( $billing_period, $expiry_date = "now" ) {
		# default is one second
		$diff = new DateInterval( "PT1S" );

		switch( self::getPeriod( $this->billingPeriod ) ) {
		case "monthly":
			$diff = new DateInterval( "P1M" );
		case "yearly":
			$diff = new DateInterval( "P1Y" );
		}

		return intval( new DateTime( $expiry_date ) )->add( $diff )->format( "U" );
	}

	/**
	 * Removes the groups from the given user.
	 *
	 * @param $user_id
	 */
	private static function removeUserFromGroups( $user_id ) {
		$user = User::newFromId( $user_id );
		$groups = array_keys(
			MediaWikiServices::getInstance()
			->getMainConfig()->get( "WSAuthorizeMemberships" )
		);

		foreach ( $groups as $group ) {
			$user->removeGroup( $group );
		}
	}

	/**
	 * Assigns the user to the given groups.
	 *
	 * @param $user_id
	 */
	private static function assignUserToGroups( array $groups, $expiry, $user_id ) {
		$user = User::newFromId( $user_id );

		foreach ( $groups as $group ) {
			$user->addGroup( $group, $expiry );
		}
	}

	/**
	 * Renews the expiry_date column in the database.
	 *
	 * @param $expiry_date
	 * @param $user_id
	 */
	private static function renewDatabaseEntry( $expiry_date, $user_id ) {
		wfGetDB( DB_MASTER )->update(
			'mwstake_members',
			[
				'expiry_date' => $expiry_date
			],
			[
				'user_id' => $user_id,
				'is_cancelled' => false
			]
		);
	}

	private static function getGroupsForUser( User $user ) {
		$groups = $user->getGroups();
		$memberships = array_keys(
			MediaWikiServices::getInstance()
			->getMainConfig()->get( 'WSAuthorizeMemberships' )
		);

		return array_intersect( $memberships, $groups );
	}

	/**
	 * @see https://stackoverflow.com/a/19680778
	 *
	 * @param $seconds
	 * @return string
	 * @throws Exception
	 */
	private function secondsToTime( $seconds ) {
		$dtF = new DateTime( '@0' );
		$dtT = new DateTime( "@$seconds" );
		return $dtF->diff( $dtT )->format(
			'%a days, %h hours, %i minutes and %s seconds'
		);
	}
}

$maintClass = RenewMembership::class;
require_once RUN_MAINTENANCE_IF_MAIN;
